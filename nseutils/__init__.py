import json

def read_configuration(path="/root/atom-projects/nuncaseesqueca/config.json"):
	config = {}
	with open(path, 'r') as f:
		config = json.loads(f.read())
	return config

print("reading config")
config = read_configuration()
MONGO_URI = config['mongo_uri']