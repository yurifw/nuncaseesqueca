import json
from google.cloud import language_v1
from google.oauth2 import service_account
import pymongo

from . import read_configuration, MONGO_URI

def read_config(path="/root/atom-projects/nuncaseesqueca/config.json"):
	return read_configuration(path)

def tags_suggestion(text):
	json_path = read_config()['google_natural_language_api_auth_json']
	credentials=service_account.Credentials.from_service_account_file(json_path)
	client = language_v1.LanguageServiceClient(credentials=credentials)
	type_ = language_v1.Document.Type.PLAIN_TEXT
	text_content = text
	encoding_type = language_v1.EncodingType.UTF8
	language = "pt"
	document = {"content": text_content, "type_": type_, "language": language}
	response = client.analyze_entities(request = {'document': document, 'encoding_type': encoding_type})
	entities = []
	for entity in response.entities:
		if entity.metadata and 'wikipedia_url' in entity.metadata:
			entities.append(entity.name)
	return entities

def get_db():
	client = pymongo.MongoClient(MONGO_URI)
	db = client.nse
	return db

