import json
from bson import ObjectId
from datetime import datetime

class MongoJsonEncoder(json.JSONEncoder):
	def default(self, o):
		if isinstance(o, ObjectId):
			return str(o)
			# return {"_id": str(o)}
		if isinstance(o, datetime):
			return o.strftime("%Y-%m-%d %H:%M:%S")
		return json.JSONEncoder.default(self, o)
