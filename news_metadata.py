# https://platform.openai.com/docs/guides/fine-tuning
# https://platform.openai.com/docs/guides/fine-tuning/preparing-your-dataset
import pymongo
import os
import random
import tiktoken

"""
aggregation to count memories by date
[
  {"$match": {"scraped_date": {
    "$gte": ISODate("2023-06-20T00:00:00.000Z"),
    "$lt": ISODate("2023-06-20T23:59:59.000Z")
  }}},
  {
    "$count": "count"
  }
]

average of 200 memories per day
"""


def load_untrained_memories(db):
	memories = db.scrapedMemories.aggregate([
		{"$lookup":	{
			"from": 'fineTunningData',
			"localField": '_id',
			"foreignField": 'memory_id',
			"as": 'training'
		}},
		{"$match": {
			"$expr": {
				"$eq": [0, {"$size": "$training"}]
			}
		}},
		{"$project": {"training":0}}
	])
	return list(memories)


def ask_for_tags():
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).nuncaseesqueca
	memories = load_untrained_memories(db)

	random.shuffle(memories)
	for memory in memories:
		os.system('cls')

		print(f"url: {memory['url']}\n")
		print("headline:")
		print(memory['headline'])
		print("\ncontent:")
		print(memory['text'])
		
		instructions = "\nenter tags separated by comma. c to skip, enter to save tags\n"
		tags = input(instructions)
		if tags == 'c':
			return None
		tags = tags.split(',')
		tags = [tag.strip().lower() for tag in tags]
		
		if tags is not None:
			memory['tags'] = tags
			memory['memory_id'] = memory['_id']
			del memory['_id']
			db.fineTunningData.insert_one(memory)
			print("tags saved")
			input("press enter to continue")

def crop_text(text, percentage=0.2, minimum_chars=300):
	text = text[:int(len(text)*percentage)]
	if len(text) < minimum_chars:
		text = text[:minimum_chars]
	return text

def show_cropped_news(percentage, minimum_chars):
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).old_nse
	memories = db.newMemories.find({})
	memories = list(memories)
	for memory in memories:
		text = memory['text']
		text = text[:int(len(text)*percentage)]
		if len(text) < minimum_chars:
			text = memory['text'][:minimum_chars]
		print(f"\nurl: {memory['url']}\n")
		print("\ntags:")
		print(memory['tags'])
		print("\nheadline:")
		print(memory['headline'])
		print("\ncontent:")
		print(text)
		input("\npress enter to continue")
		os.system('cls')


def get_last_occurrence_of_tags(tags, text):
	percentage = 0  # percentage of text that has to be read until the last tag appears'
	character_count = 0  # number of characters that have to be read until the last tag appears
	for tag in tags:
		try:
			tag_pos = text.index(tag)+len(tag)
			character_count = max(character_count, tag_pos) 
			tag_percentage = tag_pos / len(text)
			percentage = max(percentage, tag_percentage)
		except ValueError:
			pass
	return percentage, character_count


def get_averages():
	
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).old_nse
	encoding = tiktoken.encoding_for_model("curie")

	memories = db.newMemories.find({})
	memories = list(memories)
	average_size = 0
	total_tokens = 0
	average_percentage = 0
	average_character_count = 0
	for memory in memories:
		text = memory['text']
		average_size += len(text)
		tokens_integer=encoding.encode(text)
		total_tokens += len(tokens_integer)
		percentage, character_count = get_last_occurrence_of_tags(memory['tags'], text)
		average_percentage += percentage
		average_character_count += character_count


	average_size /= len(memories)
	average_tokens = total_tokens / len(memories)
	print(f"average size: {average_size} characters")
	print(f"average tokens: {average_tokens} tokens")
	print(f"average tokens per character: {average_tokens/average_size}")
	print(f"total tokens: {total_tokens} tokens")
	print(f"average percentage: {average_percentage/len(memories)}")
	print(f"average character count: {average_character_count/len(memories)}")

def calculate_relevance_price():
	"""this method tries to calculate how much you'll spend by checking if the news is relevant.
	
	aggregation toal news by date
	[
		{"$match": 
			{"scraped_date": 
			{$gte:ISODate("2023-06-27"), "$lt":ISODate("2023-06-28")}
			
			}
		},
		{$count: "total"}
	]
"""

# approx 40 cents per training with 100 news with CURIE model
if __name__ == '__main__':
	ask_for_tags()