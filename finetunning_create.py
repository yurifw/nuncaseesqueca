# https://platform.openai.com/docs/guides/fine-tuning
# https://platform.openai.com/docs/guides/fine-tuning/preparing-your-dataset
import pymongo
import os
import json
import pymongo
import openai
import codecs

openai.api_key = os.environ['OPENAI_KEY']

def crop_text(text, percentage=0.2, minimum_chars=300):
	text = text[:int(len(text)*percentage)]
	if len(text) < minimum_chars:
		text = text[:minimum_chars]
	return text

def generate_data(output_file):
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).old_nse
	memories = db.newMemories.find({})[0:100]
	file_content = []
	for memory in memories:
		text = crop_text(memory['text'])
		completion = ";".join(memory['tags']) + "\n"
		file_content.append({
			"prompt": f"headline: {memory['headline']}\ntext: {text}\n\n###\n\n",
			"completion": completion
		})
	with codecs.open(output_file, 'w', encoding='utf-8') as f:
		for line in file_content:
			f.write(json.dumps(line, ensure_ascii=False) + '\n')
	print("file generated, you can validate the file with 'openai tools fine_tunes.prepare_data -f <file_name>' ")

def upload_training_file(file_path):
	upload_response = openai.File.create(
		file=open(file_path, "rb"),
		purpose='fine-tune'
	)
	return upload_response

def list_uploaded_files():
	print(openai.File.list())

def delete_file(file_id):
	print(openai.File.delete(sid=file_id))

def list_finetunning_progress():
	print(openai.FineTune.list())

def list_finetunning_models():
	all_models = dict(openai.FineTune.list())
	print(all_models)
	

def test_model():
	headline = "Sistema financeiro pode mais no combate à lavagem de gado"
	text = """O combate ao desmatamento ilegal na Amazônia exige a articulação de esforços dos mais diversos atores da sociedade brasileira. Nesse sentido, é importante a iniciativa dos bancos nacionais para elevar a régua de concessão de crédito a frigoríficos. Mas as instituições financeiras podem e devem desempenhar um papel verdadeiramente ativo no combate ao desmatamento ilegal com a aplicação de técnicas já utilizadas rotineiramente por seus departamentos de controle e conformidade, não só para os tomadores de crédito, mas também para seus correntistas.Os mundialmente conhecidos processos de "know your customer" ("conheça seu cliente") e "know your transactions" (registro de operações de pagamento, recebimento e transferência de recursos), já disciplinados pelo Banco Central para a prevenção da lavagem de dinheiro e financiamento do """
	prompt = f"{headline}\n{text}\n\n###\n\n",
	fine_tuned_model = "babbage:ft-personal-2023-07-04-15-49-31"
	answer = openai.Completion.create(
		model=fine_tuned_model,
		prompt=prompt,
		temperature=0,
		stop=["---"]
	)
	print(answer['choices'][0]['text'])

def delete_finetunning(fine_tunning_id):
	print(openai.Model.delete(sid=fine_tunning_id))


def create_finetunning():
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).old_nse
	output_file = 'tagger_finetunning.jsonl'
	memories = db.newMemories.find({})
	memories = list(memories)[0:1500]
	file_content = []

	for memory in memories:
		body = memory['text']
		body = body[:int(len(body)*0.17)]
		text = f"{memory['headline']}\n{body}\n\n###\n\n"
		completion = ",".join(memory['tags']) + "---"
		file_content.append({
			"prompt": text,
			"completion": completion
		})
	
	with codecs.open(output_file, 'w', encoding='utf-8') as f:
		for line in file_content:
			f.write(json.dumps(line, ensure_ascii=False) + '\n')
	file_path = input(f"file generated: {output_file}, you can validate the file with 'openai tools fine_tunes.prepare_data -f <file_name>' after you validate it paste the new file name here\n")
	
	upload_response = openai.File.create(
		file=open(file_path, "rb"),
		purpose='fine-tune'
	)
	print("file uploaded, response:")
	print(upload_response)

	file_id = upload_response['id']

	r = openai.FineTune.create(training_file=file_id, model='babbage')
	print("fine tuning created:")
	print(r)


if __name__ == '__main__':
	test_model()