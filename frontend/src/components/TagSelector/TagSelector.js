import React, { useState, useEffect } from 'react';
import CreatableSelect from 'react-select/creatable'; //https://github.com/JedWatson/react-select
import reactSelectStyles from "../ReactSelectStyles.js"
import {axios} from '../../AxiosInstance.js'

function mapStringArray(arr){
	if (arr) return arr.map((tag)=>{
		return {"label":tag, "value":tag}
	})
}

function mapObjectArray(arr){
	return arr.map((tag)=>{
		return tag.value
	})
}

export default function TagSelector (props) {
	const [tagOptions, setTagOptions] = useState([])


	useEffect(()=>{
		if (props.tagOptions){
			setTagOptions(mapStringArray(props.tagOptions))
		} else {
			axios.get("/tag").then((resp)=>{
				var mapped = mapStringArray(resp.data.data)
				setTagOptions(mapped)
			})
		}
	}, [])

	// console.log("props.selectedTags", props.selectedTags)
	// console.log("props.initializeWith", props.initializeWith)
	// console.log("mapStringArray(props.initializeWith)", mapStringArray(props.initializeWith))
	// console.log("tagOptions", tagOptions)
	return (
		<CreatableSelect
			name="tags"
			defaultValue={mapStringArray(props.initializeWith)}
			styles={reactSelectStyles}
			onChange={(tags) => {props.onChange(mapObjectArray(tags))}}
			placeholder="Etiquetas"
			options={tagOptions}
			isMulti={true}/>
	)

}