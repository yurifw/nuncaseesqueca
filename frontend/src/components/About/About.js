import React from "react";
import '../../index.css';
import './About.css';
import Menu from '../Menu/Menu.js'
import Header from '../Header/Header.js'

export default class About extends React.Component {
	render() {
		return (
			<div>
				<Header />
				<Menu />
				<div className="contanier-sobre">

					<div className="section-title">	O quê? 	</div>
					<div className="section-content">
						<p>
						Nós temos o objetivo de nos tornarmos um memorial dos principais acontecimentos do país.
						Queremos nos tornar uma ferramenta para organizar e catalogar todas as notícias que alguém julgar importante.
						Queremos possibilitar pesquisar notícias em diversas fontes
						e traçar uma linha do tempo sobre qualquer entidade política
						</p>
					</div>

					<div className="section-title">	Como? </div>
					<div className="section-content">
						<p>
							Para alcançar nossos objetivos contamos com a população,
							todas as memórias serão salvas pelos usuários, assim como a moderação das mesmas
						</p>
					</div>

					<div className="section-title"> Porquê? </div>
					<div className="section-content">
						<p>
							Para alcançar nossos objetivos contamos com a população,
							todas as memórias serão salvas pelos usuários, assim como a moderação das mesmas
						</p>
					</div>

					<div className="section-title"> Onde? </div>
					<div className="section-content">
						<p>
						Para alcançar nossos objetivos contamos com a população,
						todas as memórias serão salvas pelos usuários, assim como a moderação das mesmas
						</p>
					</div>
					<div className="section-title"> Quando? </div>
					<div className="section-content">
						<p>
							Para alcançar nossos objetivos contamos com a população, todas as memórias serão salvas pelos usuários, assim como a moderação das mesmas
						</p>
					</div>
				</div>
			</div>
		);
	}
}