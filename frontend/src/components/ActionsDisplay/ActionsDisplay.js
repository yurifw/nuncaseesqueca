import React from 'react';
import './ActionsDisplay.css';
import {formatMongoDateString} from '../../utils.js'

export default class ActionsDisplay extends React.Component {

	constructor(props){
		super(props)
	}

	render(){
		return this.props.actions.map((action, index) => {
			return <div key={index} className="action-container">
				<div className={action.modifier > 0? "positive-modifier":"negative-modifier"}>
					{action.modifier > 0? "+"+action.modifier:action.modifier}
				</div>
				<div className="action-description">
					<a href={"/memoria/"+(action.meta.targetMemory || action.meta.resultingMemory)}>{action.description}</a>
				</div>
				<div className="action-date">
					{formatMongoDateString(action.date)}
				</div>
			</div>
		})
	}


}