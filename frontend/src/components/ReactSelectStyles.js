const height = 30;

const reactSelectStyles = {
	option: (provided, state) => ({
		paddingLeft: '5px',
		backgroundColor: state.isFocused?"#ffffff":"#beb9b9",
		borderWidth: '0px'
	}),
	container: (provided, state) => ({
		...provided,
		padding: '0px 0px 0px 0px',
		backgroundColor: 'transparent',
		// margin: '10px 0px 5px 0px',
		borderWidth: '1px'
	}),
	control: (provided, state) => ({
		...provided,
		padding: 0,
		backgroundColor: 'transparent',
		borderWidth: '0px 0px 1px 0px',
		borderRadius: '0px',
		borderColor: '#000000',
		boxShadow: 'none',
		height: height,
		minHeight: height
	}),
	menuList: (provided, state) => ({
		...provided,
		padding:0,
		margin:0,
		borderRadius: '0px',
	}),
	menu: (provided, state) => ({
		...provided,
		padding:0,
		margin:0,
		borderRadius: '0px',
	}),
	multiValue: (provided, state) => ({
		...provided,
		backgroundColor: '#beb9b9',
		color: "#000000",
	}),
	multiValueLabel: (provided, state) => ({
		...provided,
		padding: 0,
	}),
	dropdownIndicator: (styles) => ({
	    ...styles,
		height: height,
	    paddingTop: 0,
	    paddingBottom: 0,
	}),
	clearIndicator: (styles) => ({
	    ...styles,
		height: height,
	    paddingTop: 0,
	    paddingBottom: 0,
	}),
	valueContainer: (styles) => ({
	    ...styles,
		height: height,
	    paddingTop: 0,
	    paddingBottom: 0,
	}),
	placeholder: (styles) => ({
	    ...styles,
		// fontWeight:600,
		fontSize:'90%'
	}),
}

export default reactSelectStyles;