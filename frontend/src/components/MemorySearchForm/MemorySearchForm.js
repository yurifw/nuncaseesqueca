import React from "react";
import FormComponent from '../FormComponent';
import TagSelector from '../TagSelector/TagSelector.js';
import './MemorySearchForm.css';
import {axios} from '../../AxiosInstance.js'
import MemoryCollection from '../MemoryCollection/MemoryCollection.js'
import { withRouter } from 'react-router-dom';
import Menu from '../Menu/Menu.js'
import Header from '../Header/Header.js'


export class MemorySearchForm extends FormComponent {
	constructor(props){
		super(props)
		this.state = {
			showLoadingIcon: false,
			allTags:[],
			initialAllTags: null,
			sortBy:'new',
			searchResult: []
		}
	}

	componentDidMount(){
		const params = new URLSearchParams(this.props.location.search);


		if(!this.props.location.search){
			this.setState({initialAllTags:[]})
			return
		}

		var sort = params.get('sort')?params.get('sort'):this.state.sortBy

		this.setState({
			initialAllTags:params.get('tags_all').split(","),
			allTags:params.get('tags_all').split(","),
			sortBy: sort
		}, ()=>{this.updateSearchUrl()})

	}

	filtersToURL(){
		var url=""
		if (this.state.allTags.length > 0){
			url += 'tags_all='
			url += this.state.allTags.join(',') + '&'
		}
		if (this.state.sortBy == 'old'){
			url += 'sort=old&'
		}
		url = url.slice(0, -1)

		return url
	}

	updateSearchUrl(){
		var parameters = this.filtersToURL()
		if(this.props.onSearchParemetersChange){
			this.props.onSearchParemetersChange(parameters)
		}

	}

	renderSearchTag(){
		if(this.state.initialAllTags === null){
			return (
				<div className="filter-tag"> Carregando ... </div>
			)
		}
		return (
			<div className="filter-tag">
				<div>Memórias que incluem todas as etiquetas seguintes:</div>
				<TagSelector
					initializeWith = {this.state.initialAllTags}
					selectedTags = {this.state.allTags}
					onChange={(tags) => {this.setState({"allTags":tags})}}	/>
			</div>
		)
	}

	renderSorting(){
		if (this.props.showSorting){
			return (
				<div className="filter-sort">
					<div>Ordenar resultados:</div>
					<input
						type="radio" value="new" name="sortBy"
						checked={this.state.sortBy === "new"}
						onChange={this.handleInputChange}/>
						Mais recentes primeiro
				    <input type="radio" value="old" name="sortBy"
						checked={this.state.sortBy === "old"}
						onChange={this.handleInputChange}/>
						Mais antigos primeiro
				</div>
			)
		}
	}

	render(){
		return <div className="filter-panels">
			{this.renderSearchTag()}
			{this.renderSorting()}
			<button
				className="custom-button"
				onClick={()=>{this.updateSearchUrl()}}>
				<i className="fas fa-search"></i> Pesquisar</button>
		</div>
	}
}

export default withRouter(MemorySearchForm)