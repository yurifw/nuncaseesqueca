import Menu from '../Menu/Menu.js'
import FormComponent from '../FormComponent';
import './SignUp.css';
import {axios} from '../../AxiosInstance.js'
import { withAlert } from 'react-alert'
import { withRouter } from 'react-router-dom';

class SignUp extends FormComponent {
	constructor(props){
		super(props)
		this.state = {
			...this.state,
			"username":"",
			"password":"",
			"password2":"",
			"email":""
		}
	}

	save(){
		const alert = this.props.alert;
		const history = this.props.history;

		if(this.state.password !== this.state.password2){
			alert.error("As senhas não conferem")
			return
		}
		axios.post('/user', {
			"username":this.state.username,
			"password":this.state.password,
			"email":this.state.email
		}).then((resp) => {
			console.log(resp)
			if(resp.data.success){
				localStorage.setItem("jwt",resp.data.data)
				history.push('/memorias')
			}
		})

		console.log("saved")
	}

	render() {
		return (
			<div className="container-cadastro">
				<div className="form-wrapper">
					<input type="text" className="custom-txt" placeholder="Usuário" name="username" onChange={this.handleInputChange} value={this.state.username}/>
					<input type="text" className="custom-txt" placeholder="Email"  name="email" onChange={this.handleInputChange}  value={this.state.email}/>
					<input type="password" className="custom-txt" placeholder="Senha"  name="password" onChange={this.handleInputChange}  value={this.state.password}/>
					<input type="password" className="custom-txt" placeholder="Confirme a Senha"  name="password2" onChange={this.handleInputChange}  value={this.state.password2}/>
					<button className="custom-button" onClick={()=>{this.save()}}> <i className="fas fa-sign-in-alt"></i> Cadastrar</button>
				</div>
			</div>
		);
	}
}

export default withRouter(withAlert()(SignUp))