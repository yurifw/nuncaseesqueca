import React from "react";
import { Link } from "react-router-dom"; //https://reactrouter.com/web/guides/quick-start
import '../../index.css';
import './Header.css';

export default class Menu extends React.Component {
	render() {
		return (
			<div className="title-wrapper">
				<h1 className="title">Nunca se Esqueça</h1>
				<h2 className="subtitle">Um memorial a tudo que machucou a memória do Brasil.</h2>
			</div>
		);
	}
}