import React from "react";
import '../../index.css';
import './VisualizationPage.css';
import {Collapse} from 'react-collapse';  // https://www.npmjs.com/package/react-collapse
import MemorySearchForm from '../MemorySearchForm/MemorySearchForm.js'
import VisualizationGraph from '../VisualizationGraph/VisualizationGraph.js'
import {axios} from '../../AxiosInstance.js'
import { withRouter } from 'react-router-dom';

export class VisualizationPage extends React.Component {
	constructor(props){
		super(props)
		this.state={
			showingFilters:false,
			searchUrl:"",
			memories:[],
			firstSearch: true,
		}
		this.onSearchParemetersChange = this.onSearchParemetersChange.bind(this)
	}

	onSearchParemetersChange(newUrl){
		this.props.history.push({
			pathname: '/visualizacao',
			search: "?" + newUrl
		})
		if(!this.state.firstSearch){
			this.props.history.go(); //needs to refresh the page because cytoscape only arranjes the nodes the first time data is loaded, if you remove this all nodes will be overlapping on the next search
		}
		var params = newUrl + "&offset=0&limit=300"
		axios.get("/memory?"+params).then((resp) =>{
			if (resp.data.success){
				this.setState({
					memories: resp.data.data,
					firstSearch: false
				})
			}
		})
	}

	renderFilterToggleIcon(){
		if(this.state.showingFilters) return <i className="fas fa-chevron-up"></i>
		return <i className="fas fa-chevron-down"></i>
	}

	renderGraph(){
		if (this.state.memories.length > 0){
			return <VisualizationGraph memories = {this.state.memories} />
		} else {
			return <div>Nenhuma Memória encontrada</div>
		}
	}

	render() {
		return (
			<div className="visualization-page-container">
				<div className="filter-container">
					<MemorySearchForm onSearchParemetersChange={this.onSearchParemetersChange}/>
				</div>
				<div className="visualization-container">
					{this.renderGraph()}
				</div>

			</div>
		);

		return (
			<div className="visualization-page-container">
				<div className="floating-div">
					<Collapse isOpened={this.state.showingFilters}>
						<div className="filter-container">
							<MemorySearchForm onSearchParemetersChange={this.onSearchParemetersChange}/>
						</div>
					</Collapse>
					<div className="filter-toggle" onClick={()=>{this.setState({showingFilters: !this.state.showingFilters})}}>
						{this.renderFilterToggleIcon()}
					</div>
				</div>
				<div className="visualization-container">
					{this.renderGraph()}
				</div>
			</div>
		);
	}
}

export default withRouter(VisualizationPage)