import React from "react";
import {axios} from '../../AxiosInstance.js'
import CytoscapeComponent from 'react-cytoscapejs';  // https://github.com/plotly/react-cytoscapejs
import Cytoscape from 'cytoscape';
import Cola from 'cytoscape-cola';

Cytoscape.use(Cola);

export default class VisualizationGraph extends React.Component {
	constructor(props){
		super(props)
	}

	permutateTags(tags){
		var permutations = []
		while(tags.length > 0){
			var first = tags.pop()
			tags.forEach(second => {
				permutations.push([first, second])
			})
		}
		return permutations

	}

	render(){
		if(this.props.memories.length == 0){
			return <div>Sem dados</div>
		}
		var elements = [
			// { data: { id: 'one', label: 'Node 1' }, position: { x: 50, y: 50 } },
			// { data: { id: 'two', label: 'Node 2' }, position: { x: 150, y: 50 } },
			// { data: { source: 'one', target: 'two', label: 'Edge from Node1 to Node2' } },
			// { data: { source: 'one', target: 'two', label: 'Edge from Node1 to Node2' } }
		];

		var occurrences = {}
		var edges = []
		var colors = {}
		this.props.memories.forEach((memory) => {
			memory.tags.forEach(tag => {
				if (tag in occurrences) {
					occurrences[tag]['weight'] = occurrences[tag]['weight']+1
				} else {
					var randomColor = "#000000".replace(/0/g,function(){return (~~(Math.random()*16)).toString(16);});
					colors[tag] = randomColor
					occurrences[tag] = {id: tag, label:tag, weight:1, color:randomColor}
				}
			})

			var permutations = this.permutateTags(memory.tags)
			var links = permutations.map((comb, i) => {
				return { data: { source: comb[0], target: comb[1], colors: colors[comb[0]]+" "+colors[comb[1]] } }
			})
			edges = edges.concat(links)
		});
		elements = elements.concat(Object.entries(occurrences).map(pair =>{
			return {
				"data": {
					"id": pair[1].id,
					"label": pair[1].label,
					"weight": pair[1].weight,
					"color": pair[1].color
				}
			}
		}))
		elements = elements.concat(edges)

		var layout = { //this object is equivalent to the options, see builtin layouts here: https://js.cytoscape.org/#layouts
			name: 'cose',
			fit: false
		}
		// a styling example: https://stackoverflow.com/questions/64271406/cytoscape-js-show-hide-labels-on-elements-in-the-graph-on-button-click
		var sizeMultiplier = 10
		var minimumSize = 30
		var maximumSize = 80
		var clamp = (n) => {return Math.min( maximumSize, Math.max(minimumSize, n))}
		return <CytoscapeComponent
			elements={elements}
			style={ { width: '100%', height: '100%' } }
			layout={layout}
			stylesheet={[
				{
					selector: 'label',
					style:{
						// 'label':'data(label)',
						'label': (ele) => {
							if(ele.isNode()) return ele.data('label');
							if(ele.isEdge()) return null;
						},
						'text-background-color': 'white',
						"text-background-opacity" : 0,
						'background-color': 'data(color)',
						'height': (ele) =>{
							if(ele.isNode()) {
								return clamp(ele.data('weight') * sizeMultiplier)+'px'
							}
							return '1px'
						},
						'width': (ele)=>{
							if(ele.isNode()) {
								return clamp(ele.data('weight') * sizeMultiplier)+'px'
							}
							return '1px'
						}
					}
				},
				{
					selector: 'edge',
					style:{
						// 'curve-style': 'haystack',  //haystack might also look good, but you need a different data structure for the edge object
						'curve-style': 'bezier',
						'line-fill':'linear-gradient',
						'line-gradient-stop-colors': 'data(colors)',
						'line-gradient-stop-positions': '0% 100%',
					}
				}
			]}
			/>;
	}
}