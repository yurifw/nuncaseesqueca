import React from 'react';
import './UserPage.css';
import {axios} from '../../AxiosInstance.js'
import ActionsDisplay from '../ActionsDisplay/ActionsDisplay.js';

export default class UserPage extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			user: {actions:[]}
		}
	}

	componentDidMount(){
		axios.get('/user/'+this.props.match.params.username).then((resp) =>{
			if (resp.data.success){
				this.setState({
					"user":resp.data.data
				})
			}
			console.log(resp.data.data)

		})
	}

	renderHeader(){
		var headerContent = []
		if(this.state.user){
			headerContent.push(<span key="1">{this.state.user.username}</span>)
			if(this.state.user.email){
				headerContent.push(<span key="2"> - {this.state.user.email}</span>)
			}
		}
		return headerContent
	}

	render(){
		return <div className="user-profile-page">
			<div className="profile-header">{this.renderHeader()}</div>
			<div className="profile-actions-container">
				<ActionsDisplay actions={this.state.user.actions}/>
			</div>
		</div>
	}
}