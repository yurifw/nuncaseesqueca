import React from 'react';
import Menu from '../Menu/Menu.js'
import Header from '../Header/Header.js'
import './MemoryDetails.css';
import {axios} from '../../AxiosInstance.js'
import { format, parse } from 'date-fns'
import {formatMongoDateString} from '../../utils.js'
import {Collapse} from 'react-collapse';  // https://www.npmjs.com/package/react-collapse
import TagList from '../TagList/TagList.js';

export default class MemoryDetails extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			memory: null,
			collapsePanels:[]
		}
		this.renderVersion = this.renderVersion.bind(this)
		this.togglePanelState = this.togglePanelState.bind(this)
	}

	componentDidMount(){
		axios.get('/memory/'+this.props.match.params.id+'/versions').then((resp) =>{
			if (resp.data.success){
				this.setState({
					"memory":resp.data.data,
					"collapsePanels": resp.data.data.versions.map((v)=>{return false})
				})
			}
		})
	}

	togglePanelState(index){
		var collapsePanels = [...this.state.collapsePanels]
		collapsePanels[index] = !collapsePanels[index]
		this.setState({"collapsePanels":collapsePanels})
	}

	renderUserComment(version){
		if(version.userComment){
			return <div className="v-user-comment">editado por <a href={"/usuario/"+version.modifiedBy}>{version.modifiedBy}</a>:<span> {version.userComment}</span></div>
		}
		return <div className="v-user-comment">editado por <a href={"/usuario/"+version.modifiedBy}>{version.modifiedBy}</a></div>
	}

	renderCollapseIcon(isOpen){
		if(isOpen){
			return <span>
				<i className="fas fa-chevron-down"></i>
			</span>
		} else {
			return <span>
				<i className="fas fa-chevron-right"></i>
			</span>
		}
	}

	renderMergedMemories(){
		if(this.state.memory.mergedMemories){
			console.log("this.state.memory.mergedMemories",this.state.memory.mergedMemories)
			var memoryA = this.state.memory.mergedMemories[0]
			var memoryB = this.state.memory.mergedMemories[1]
			return <div className="memory-details-merged">
				Memórias combinadas: <a href={"/memoria/"+memoryA}>{memoryA}</a> e <a href={"/memoria/"+memoryB}>{memoryB}</a>
			</div>
		}
		return <div></div>
	}

	renderMergedInto(){
		if(this.state.memory.status == 'merged'){
			return <div className="memory-details-mergedinto">
				<div className="version-sys-comment">Memória Duplicada</div>
				Esta memória estava duplicada e foi combinada para formar a memória <a href={"/memoria/"+this.state.memory.mergedInto}>{this.state.memory.mergedInto}</a>
			</div>
		}
		return <div></div>
	}

	renderVersion(version, index){
		return (
		<div key={index} className="version-container">
			<div className="version-header" onClick={()=>{this.togglePanelState(index)}}>
				<div className="version-sys-comment">{version.systemComment}</div>
				<div className="version-date">{formatMongoDateString(version.modificationDate)}</div>
				<div className="version-details-toggle">
					{this.renderCollapseIcon(this.state.collapsePanels[index])}
				</div>

			</div>
			<Collapse isOpened={this.state.collapsePanels[index]}>
			<div className="version-collapsed">
				{this.renderUserComment(version)}
				<div className="v-sources">
					Fontes:
					<ul>
					{version.sources.map((s,i) =>{
						return (
							<li key={i}>
							<a href={s.url}>{s.headline}</a> Publicado {formatMongoDateString(s.publicationDate)}
							</li>
						)
					})}
					</ul>
				</div>
				<TagList tags={version.tags}/>
			</div>

			</Collapse>
		</div>
		)
	}

	render(){
		if (this.state.memory === null){ return <> </>}
		return(
		<div>
			<Header />
			<Menu />
			<div className="memory-details-container">
				<div>Criada por <a href={"/usuario/"+this.state.memory.createdBy}>{this.state.memory.createdBy}</a> {format(parse(this.state.memory.creationDate, 'yyyy-MM-dd HH:mm:ss', new Date()), 'dd/MM/yyyy HH:mm:ss')}</div>
				{this.renderMergedMemories()}
				{this.state.memory.versions.map(this.renderVersion)}
				{this.renderMergedInto()}
			</div>
		</div>
		)
	}
}