import React from "react";
import '../../index.css';
import './home.css';
import Menu from '../Menu/Menu.js'
import Header from '../Header/Header.js'

export default class Home extends React.Component {
	render() {
		return (
			<div className="container">
				<Header />
				<Menu />
			</div>
		);
	}
}