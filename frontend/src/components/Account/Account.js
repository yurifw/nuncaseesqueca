import React from "react";
import '../../index.css';
import './Account.css';
import FormComponent from '../FormComponent';
import ActionsDisplay from '../ActionsDisplay/ActionsDisplay.js';
import ScrapedMemoriesDisplay from '../ScrapedMemoriesDisplay/ScrapedMemoriesDisplay.js';
import PubSub from 'pubsub-js'
import {Collapse} from 'react-collapse';  // https://www.npmjs.com/package/react-collapse
import {axios} from '../../AxiosInstance.js'
import { withAlert } from 'react-alert'
import { withRouter } from 'react-router-dom';
import Menu from '../Menu/Menu.js'
import Header from '../Header/Header.js'


class Account extends FormComponent {

	constructor(props){
		super(props)
		this.state = {
			"email":"",
			"reputation":0,
			"novaSenha":"",
			"novaSenha2":"",
			"senhaAntiga":"",
			"showingActions":false,
			"actions":[]
		}
		this.toggleActions = this.toggleActions.bind(this)
		this.updateRep = this.updateRep.bind(this)

	}

	componentDidMount(){
		axios.get('/login/verify').then((resp) => {
			this.setState({"reputation": resp.data.data.reputation})
		})

		axios.get('/user').then((resp) => {
			this.setState({
				"email": resp.data.data.email||"",
				"actions": resp.data.data.actions
			})
		})
	}

	updateRep(){
		axios.get('/login/refresh').then((resp) => {
			localStorage.setItem("jwt",resp.data.data)
			window.location.reload(false);
		})
	}

	toggleActions(){
		this.setState({showingActions:!this.state.showingActions})
	}

	save(){
		const alert = this.props.alert;
		if(this.state.novaSenha !== this.state.novaSenha2){
			alert.error("As senhas digitadas são diferentes")
			return
		}
		var payload = {
			"newPassword":this.state.novaSenha,
			"oldPassword":this.state.senhaAntiga,
			"email":this.state.email
		}
		axios.patch('/user', payload).then((resp) => {
			console.log(resp)
		})

	}

	logout(){
		const history = this.props.history;
		localStorage.removeItem('jwt')
		PubSub.publish('USER_AUTHENTICATED', false);
		history.push('/')
	}

	renderCollapseArrow(){
		if(this.state.showingActions){
			return <span>
				<i className="fas fa-chevron-down"></i>
			</span>
		} else {
			return <span>
				<i className="fas fa-chevron-right"></i>
			</span>
		}
	}

	render() {
		return (
			<div>
				<Header />
				<Menu />
				<div className="container-conta">
					<div className="form-wrapper-conta">
						<div className="form-line">
							<div className="form-field action-toggle" onClick={this.toggleActions}>{this.renderCollapseArrow()} Reputação: {this.state.reputation}</div>
							<div className="form-tip"> Sua reputação é calculada sempre que você realiza o login, mas você pode <span className="update-rep" onClick={this.updateRep}>clicar aqui para atualizá-la</span></div>
						</div>
						<Collapse isOpened={this.state.showingActions}>
							<ActionsDisplay actions={this.state.actions}/>
						</Collapse>
						<div className="form-line">
							<div className="form-field"> <input type="text" className="custom-txt" name="email" value={this.state.email} onChange={this.handleInputChange} placeholder="Email"/> </div>
							<div className="form-tip"> Para preservar sua privacidade, o email não é obrigatório, porém é a única maneira de recuperar sua senha. </div>
						</div>
						<div className="form-line">
							<fieldset className="form-field">
								<legend>Trocar a senha</legend>
								<input type="password" className="custom-txt" name="novaSenha" value={this.state.novaSenha} onChange={this.handleInputChange} placeholder="Nova senha"/>
								<input type="password" className="custom-txt" name="novaSenha2" value={this.state.novaSenha2} onChange={this.handleInputChange} placeholder="Confirme a nova senha"/>
								<input type="password" className="custom-txt" name="senhaAntiga" value={this.state.senhaAntiga} onChange={this.handleInputChange} placeholder="Senha antiga"/>
							</fieldset>
						</div>
						<div className="form-line">
							<div className="btn-wrapper"><button className="custom-button" onClick={()=>{this.save()}}> <i className="fas fa-save"></i> Salvar </button></div>
							<div className="btn-wrapper"> <button className="custom-button" onClick={()=>{this.logout()}}> <i className="fas fa-sign-out-alt"></i> Sair </button></div>
						</div>
					</div>
				</div>
			</div>
		)
	}
}
export default withRouter(withAlert()(Account))