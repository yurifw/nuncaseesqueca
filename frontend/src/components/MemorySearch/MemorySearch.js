import React from "react";
import FormComponent from '../FormComponent';
import TagSelector from '../TagSelector/TagSelector.js';
import './MemorySearch.css';
import {axios} from '../../AxiosInstance.js'
import MemoryCollection from '../MemoryCollection/MemoryCollection.js'
import { withRouter } from 'react-router-dom';
import {parseMongoTagToSelector} from '../../utils.js'
import Menu from '../Menu/Menu.js'
import Header from '../Header/Header.js'
import MemorySearchForm from '../MemorySearchForm/MemorySearchForm.js'

export class MemoryForm extends FormComponent {

	constructor(props){
		super(props)
		this.state = {
			showLoadingIcon: false,
			offset: 0,
			limit: 10,
			searchResult: [],
			searchUrl: ""
		}
		this.search = this.search.bind(this)
		this.searchUrlChanged = this.searchUrlChanged.bind(this)
	}

	appendPagination(url){
		url += '&'
		url += 'offset='+this.state.offset+'&'
		url += 'limit='+this.state.limit+'&'
		return url.slice(0, -1)
	}

	search(){
		console.log("searching")
		var params = this.appendPagination(this.state.searchUrl)

		axios.get("/memory?"+params).then((resp) =>{
			if (resp.data.success){
				var appendedResult = this.state.searchResult.concat(resp.data.data)
				this.setState({
					searchResult: appendedResult,
					offset: this.state.offset + this.state.limit,
					showLoadingIcon:resp.data.data.length > 0
				})
			}
		})
	}

	searchUrlChanged(url){
		console.log("url changed", url)
		this.props.history.push({
			pathname: '/busca',
			search: "?" + url
		})
		this.setState({
			searchResult:[],
			showLoadingIcon: false,
			searchUrl: url,
			offset: 0,
			limit: 10
		}, ()=>{this.search()})
	}

	renderSearchResult(){
		return <MemoryCollection
			showLoadingIcon={this.state.showLoadingIcon}
			loadNextPage = {this.search}
			memories={this.state.searchResult}/>
	}

	render() {
		return (
			<div className="search-memory-container">
				<Header />
				<Menu />
				<div className="filter-panels-container">
					<MemorySearchForm
						showSorting={true}
						onSearchParemetersChange={this.searchUrlChanged}
					/>
				</div>
				{this.renderSearchResult()}
			</div>
		)
	}
}

export default withRouter(MemoryForm)