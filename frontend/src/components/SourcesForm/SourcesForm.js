import React, { useState } from 'react';
import Select from 'react-select' //https://github.com/JedWatson/react-select
import reactSelectStyles from "../ReactSelectStyles.js"
import './SourcesFormStyles.js';
import DatePicker from "react-datepicker"; //https://github.com/Hacker0x01/react-datepicker"
import "react-datepicker/dist/react-datepicker.css";
import pt from 'date-fns/locale/pt'; //https://github.com/date-fns/date-fns
import _ from "lodash";
import { format } from 'date-fns'

const sourceTypes = [
	{ value: 'jornal-online', label: 'Jornal Online' },
	{ value: 'canal-youtube', label: 'YouTube' }
]

const sourceWrapper = {
	borderStyle:"dashed",
	borderWidth: '0px 0px 1px 0px',
	margin: '5px 0px',
	display: 'flex',
	flexDirection: 'row'
}
const divForm = {
	flex:'0.95'
}
const divExcluir = {
	flex:'0.05'
}

const btnExcluir = {
	width: '100%',
	height: '100%'
}

export function parseSources(sources){
	var sourcesCopy = _.cloneDeep(sources)
	return sourcesCopy.map(s => {
		s['publicationDate'] = format(s['publicationDate'],'yyyy-MM-dd HH:mm')
		return s
	})
}

export function SourcesForm (props) {
	var fontes = props.sources
	var setFontes = props.setSources

	const handleMancheteChange = (event)=>{
		var f = fontes.map(f=>{return f})
		f[event.target.name]['headline'] = event.target.value
		setFontes(f)
	}
	const handleURLChange = (event) =>{
		var f = fontes.map(f=>{return f})
		f[event.target.name]['url'] = event.target.value
		setFontes(f)
	}
	const handleTipoChange = (newValue, target) =>{
		var f = fontes.map(f=>{return f})
		f[target.name]['type'] = newValue
		setFontes(f)
	}
	const setPublicationDate = (date, index) => {
		var f = fontes.map(f=>{return f})
		f[index]['publicationDate'] = date
		setFontes(f)
	}


	const handleExcluir = (event) =>{
		var f = fontes.filter((item, index)=>{
			return index != event.target.name
		})
		setFontes(f)
	}

	var fontesUI = []
	for(let i = 0; i<fontes.length; i++){
		fontesUI.push(
			<div key={i} style={sourceWrapper}>
				<div style={divForm}>

					{
					// <Select
					// 	onChange={(n, t)=>{handleTipoChange(n, t)}}
					// 	name={i}
					// 	value={fontes[i].type}
					// 	styles={reactSelectStyles}
					// 	options={sourceTypes}
					// 	placeholder="Tipo" />
					}
					<div>Data de Publicação:</div>
					<DatePicker
						placeholderText="Data de publicação"
						dateFormat="dd/MM/yyyy HH:mm"
						showTimeSelect
						locale={pt}
						selected={fontes[i].publicationDate}
						onChange={(date) => setPublicationDate(date, i)} />
					<input
						onChange={handleMancheteChange}
						value={fontes[i].headline}
						name={i}
						placeholder="Descrição / Manchete"
						className="custom-txt"/>
					<input
						onChange={handleURLChange}
						value={fontes[i].url}
						name={i}
						placeholder="Link"
						className="custom-txt"/>
				</div>
				<div style={divExcluir}>
					<button name={i} onClick={handleExcluir} style={btnExcluir}><i className="fas fa-trash-alt"></i></button>
				</div>


			</div>
		)
	}



	return (
		<div>
			<div>Fontes:</div>
			{fontesUI}
			<button onClick={()=>{
					setFontes([
						...fontes,
						{
							"type":sourceTypes[0],
							"headline":"",
							"url":"",
							"publicationDate":new Date()
						 }
					 ])
				}}>+</button>
		</div>
	)
}