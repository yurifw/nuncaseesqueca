
export default function TagList (props) {

	const tagContainer = {
		display: 'flex',
		flexDirection: 'row'
	}

	const tagStyle = {
		fontSize: '90%',
		backgroundColor: '#beb9b9',
		padding: '0px 5px 0px 5px',
		margin: '2px'
	}
	return (
		<div style={tagContainer}>
			{props.tags.map((tag) => {
				return <div key={tag} style={tagStyle}>{tag}</div>
			})}
		</div>
	)
}