import React, { Fragment } from "react";
import Memory from '../Memory/Memory.js'
import './MemoryCollection.css';
import ScrollTrigger from 'react-scroll-trigger';
import {axios} from '../../AxiosInstance.js'

export default class MemoryCollection extends React.Component {
	constructor(props){
		super(props)
		this.state = {
			isUserLoggedIn: false
		}
	}

	componentDidMount(){
		axios.get('/login/verify').then( (resp) => {
			this.setState({"isUserLoggedIn":resp.data.success})
		})
	}

	loadIcon() {
		if(this.props.showLoadingIcon){
			return <ScrollTrigger onEnter={()=>{this.props.loadNextPage()}} >
			<div className="loading-memories">
				<div><img className="loading-icon" src="https://thumbs.gfycat.com/ColorlessReadyGlowworm-small.gif" /></div>
				<div className="load-button-container">
					<button
						className="custom-button"
						onClick={()=>{this.props.loadNextPage()}}>
						<i className="fas fa-search"></i> Carregar mais</button>
				</div>

			</div>
			</ScrollTrigger>
		}
		return <></>
	}

	render(){
		return <Fragment>
			{this.props.memories.map((memory) =>{
				return <Memory key={memory._id} memory={memory} showEditMenu={this.state.isUserLoggedIn}/>
			})}
			{this.loadIcon()}

		</Fragment>
	}
}