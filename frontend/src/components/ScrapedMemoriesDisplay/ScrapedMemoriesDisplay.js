import React from "react";
import './ScrapedMemoriesDisplay.css';
import TagSelector from '../TagSelector/TagSelector.js';
import {axios} from '../../AxiosInstance.js'
import {parseSources} from '../SourcesForm/SourcesForm.js';
import Header from '../Header/Header.js';
import Menu from '../Menu/Menu.js';
import {Collapse} from 'react-collapse';  // https://www.npmjs.com/package/react-collapse
import FormComponent from '../FormComponent';

export default class ScrapedMemoriesDisplay extends FormComponent {

	constructor(props){
		super(props)
		this.state = {
			memories: [],
			tagOptions: [],
			showReplacements:false,
			replacerTag:"",
			replacedTag:"",
			replacements:[]
		}
	}

	componentDidMount(){
		axios.get("/memory/scraped").then((resp) => {
			if(resp.data.success){
				this.setState({memories:resp.data.data})
			}
		})
		axios.get("/tag").then((resp)=>{
			this.setState({tagOptions:resp.data.data})
		})
		axios.get("/tag/replacement").then((resp) => {
			if(resp.data.success){
				this.setState({replacements:resp.data.data})
			}
		})
	}

	saveAsMemory(memory, buttonElement){
		var payload = {
			"tags":memory.tags,
			"sources":memory.sources
		}
		console.log(payload)
		axios.post('/memory/scraped/'+memory._id+'/accept',payload).then((resp)=>{
			if(resp.data.success){
				var memoryContainer = buttonElement.target.parentElement.parentElement.parentElement
				memoryContainer.classList.toggle('dismissed')
			}
		})
	}

	addAsSource(memory, buttonElement){
		var id = prompt("Digite o ID da memória que receberá esta fonte")
		axios.post('/memory/scraped/'+memory._id+'/merge/'+id).then((resp)=>{
			if(resp.data.success){
				var memoryContainer = buttonElement.target.parentElement.parentElement.parentElement
				memoryContainer.classList.toggle('dismissed')
			}
		})
	}

	discard(memory, buttonElement){
		axios.post('/memory/scraped/'+memory._id+'/discard').then((resp)=>{
			if(resp.data.success){
				var memoryContainer = buttonElement.target.parentElement.parentElement.parentElement
				memoryContainer.classList.toggle('dismissed')
			}
		})
	}

	newReplacement(){
		var payload = {
			"replaced": this.state.replacedTag,
			"replacer": this.state.replacerTag
		}
		axios.post("/tag/replacement",payload).then((resp) => {
			if(resp.data.success){
				window.location.reload(false);
			}
		})
	}

	removeReplacement(replacement){
		var proceed = window.confirm("Deletando Substituição: "+replacement.replaced+" => "+replacement.replacer)
		if(proceed){
			axios.delete("/tag/replacement/"+replacement._id).then(resp => {
				if(resp.data.success){
					window.location.reload(false);
				}
			})
		}
	}

	renderMemories(){
		return this.state.memories.map( memory => {
			return(
				<div key={memory._id} className={memory.status=='waiting'?"scraped-memory-container":' dismissed'}>
					<div><a href={memory.sources[0].url} target="_blank">{memory.sources[0].headline}</a></div>
					<div>
						<TagSelector
							tagOptions = {this.state.tagOptions}
							selectedTags = {memory.tags}
							onChange={(tags) => {memory.tags = tags}}
							initializeWith={memory.tags} />
					</div>
					<div className="button-container">
						<div>
							<button
								className="custom-button"
								onClick={(target)=>{this.saveAsMemory(memory, target)}}>
								<i className="fas fa-save"></i> Salvar como Memória
							</button>
						</div>
						<div>
							<button
								className="custom-button"
								onClick={(target)=>{this.addAsSource(memory, target)}}>
								<i className="fas fa-plus"></i> Adicionar Fonte
							</button>
						</div>
						<div>
							<button
								className="custom-button"
								onClick={(target)=>{this.discard(memory, target)}}>
								<i className="fas fa-times"></i> Descartar
							</button>
						</div>
					</div>
				</div>
			)
		})
	}

	render(){
		return <div>
			<Header />
			<Menu />
			<div className="scraped-container">
				<div>
					<h3 onClick={()=>{this.setState({showReplacements:!this.state.showReplacements})}} style={{cursor:"pointer"}}>Substituições</h3>
					<Collapse isOpened={this.state.showReplacements}>
						<div className="new-replacement">
							<div>
								<input
									type="text"
									className="custom-txt"
									placeholder="Etiqueta substituída"
									name="replacedTag"
									onChange={this.handleInputChange}
									value={this.state.replacedTag}/>
							</div>
							<div>
								<input
									type="text"
									className="custom-txt"
									placeholder="Nova Etiqueta"
									name="replacerTag"
									onChange={this.handleInputChange}
									value={this.state.replacerTag}/>
							</div>
							<div>
								<button
									className="custom-button"
									onClick={()=>{this.newReplacement()}}>
									Adicionar</button>
							</div>
						</div>
						<div className="current-replacements">
							{this.state.replacements.map(r => {
								return <div key={r._id} onClick={()=>{this.removeReplacement(r)}} style={{cursor:"pointer"}}>
									{r.replaced} <i className="fas fa-long-arrow-alt-right"></i> {r.replacer}
								</div>
							})}
						</div>
					</Collapse>
				</div>
				<div>
					{this.state.memories.length + " Memórias na fila"}
					{this.renderMemories()}
				</div>
			</div>
		</div>

	}

}