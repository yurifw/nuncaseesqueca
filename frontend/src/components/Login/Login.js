import React from "react";
import Menu from '../Menu/Menu.js'
import Header from '../Header/Header.js'
import '../../index.css';
import './Login.css';
import FormComponent from '../FormComponent';
import PubSub from 'pubsub-js'
import { Link } from "react-router-dom";
import {axios} from '../../AxiosInstance.js'
import { withRouter } from 'react-router-dom';

class Login extends FormComponent {
	constructor(props){
		super(props)
		this.state = {
			...this.state,
			"username":"",
			"password":""
		}
	}

	login(){
		axios.post('/login', {
			"username": this.state.username,
			"password": this.state.password
		}).then((resp)=>{
			if(resp.data.success){
				localStorage.setItem("jwt",resp.data.data)
				PubSub.publish('USER_AUTHENTICATED', true);
				this.props.history.push("/memorias")
			}
		})
	}

	render() {
		return (
			<div className="container">
				<Header />
				<Menu />
				<div className="container-login">
					<div className="form-wrapper">
						<input type="text" className="custom-txt" placeholder="Usuário" name="username" onChange={this.handleInputChange} value={this.state.username}/>
						<input type="password" className="custom-txt" placeholder="Senha"  name="password" onChange={this.handleInputChange}  value={this.state.password}/>
						<button className="custom-button" onClick={()=>{this.login()}}> <i className="fas fa-sign-in-alt"></i> Login</button>

						<div><Link to="/cadastro"> Cadastrar </Link></div>
						<div> Esqueci minha senha </div>
					</div>
				</div>
			</div>
		);
	}
}

export default withRouter(Login)