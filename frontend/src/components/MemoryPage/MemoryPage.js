import React from "react";
import '../../index.css';
import './MemoryPage.css';
import FormComponent from '../FormComponent';
import Modal from 'react-modal';  // https://github.com/reactjs/react-modal
import MemoryForm from '../MemoryForm/MemoryForm.js'
import MemoryCollection from '../MemoryCollection/MemoryCollection.js'
import Menu from '../Menu/Menu.js'
import Header from '../Header/Header.js'
import {axios} from '../../AxiosInstance.js'

export default class MemoryPage extends FormComponent {
	constructor(props){
		super(props)
		this.state = {
			isUserLoggedIn: false,
			showLoadingIcon: false,
			isModalOpen: false,
			memories: [],
			offset:0,
			limit:10
		}
		Modal.setAppElement('#root')
	}

	componentDidMount(){
		axios.get('/login/verify').then( (resp) => {
			this.setState({"isUserLoggedIn":resp.data.success})
		})
		this.searchRecentMemories(this.state.offset, this.state.limit)
	}

	closeModal(){
		this.setState({"isModalOpen":false})
	}



	searchRecentMemories(offset, limit){
		axios.get("/memory?offset="+offset+"&limit="+limit).then((resp) =>{
			if (resp.data.success){
				var appendedResult = this.state.memories.concat(resp.data.data)
				this.setState({
					memories: appendedResult,
					showLoadingIcon: resp.data.data.length > 0,
					offset: this.state.offset + this.state.limit
				})
			}
		})
	}

	renderAddButton(){
		if(this.state.isUserLoggedIn){
			return <button className="custom-button" style={{width:'200px', marginLeft:'calc(50% - 100px)'}} onClick={()=>{this.setState({'isModalOpen':true})}}>
				<i className="fas fa-plus-square"></i> Adicionar Memória
			</button>
		} else{
			return <div />
		}

	}

	render() {
		return (
			<div className="container">
				<Header />
				<Menu />
				{this.renderAddButton()}
				<Modal
					isOpen={this.state.isModalOpen}
					onRequestClose={()=>{this.closeModal()}}
				>
					<MemoryForm />
				</Modal>

				<MemoryCollection
					showLoadingIcon={this.state.showLoadingIcon}
					memories={this.state.memories}
					loadNextPage={()=>{this.searchRecentMemories(this.state.offset, this.state.limit)}}/>


			</div>
		);
	}
}