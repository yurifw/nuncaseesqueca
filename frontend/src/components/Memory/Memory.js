import React from 'react';
import './Memory.css';
import { format, parse } from 'date-fns'
import { Link } from "react-router-dom"; //https://reactrouter.com/web/guides/quick-start
import {
	Menu,
	MenuItem,
	MenuButton,
	SubMenu
} from '@szhsin/react-menu'; //https://github.com/szhsin/react-menu
import '@szhsin/react-menu/dist/index.css';
import Modal from 'react-modal';  // https://github.com/reactjs/react-modal
import TagSelector from '../TagSelector/TagSelector.js';
import TagList from '../TagList/TagList.js';
import {SourcesForm, parseSources} from '../SourcesForm/SourcesForm.js';
import {axios} from '../../AxiosInstance.js'
import {Collapse} from 'react-collapse';  // https://www.npmjs.com/package/react-collapse
import FormComponent from '../FormComponent';
import { withRouter } from 'react-router-dom';
import { withAlert } from 'react-alert'
import _ from "lodash";

class Memory extends FormComponent {
	constructor(props){
		super(props)
		this.state = {
			isNewSourcesOpen: false,
			isMergingModalOpen: false,
			isEditedSourcesOpen: false,
			isNewTagsOpen:false,
			newSelectedTags: [],
			mergingMemoryId:"",
			newSources:[{
				type: {"value": "jornal-online", "label": "Jornal Online"},
				headline: "",
				url: "",
				publicationDate: new Date()
			}],
			editedSources: props.memory.sources.map( m => {
				var mCopy = _.cloneDeep(m)
				mCopy.type = {"value": mCopy.type, "label": mCopy.type}
				mCopy.publicationDate = parse(mCopy.publicationDate, 'yyyy-MM-dd HH:mm:ss', new Date())
				return mCopy
			}),
			editingComment: "",
			showingSources: false
		}

		this.copyIdToClipboard = this.copyIdToClipboard.bind(this)
		this.onNewTagsChange = this.onNewTagsChange.bind(this)
		this.setEditingComment = this.setEditingComment.bind(this)
		this.onNewSourcesChange = this.onNewSourcesChange.bind(this)
		Modal.setAppElement('#root')
	}

	onNewSourcesChange(newSources){
		this.setState({"newSources":newSources})
	}

	onNewTagsChange(tags){
		this.setState({"newSelectedTags":tags})
	}

	copyIdToClipboard(){
		navigator.clipboard.writeText(this.props.memory._id)  // this should only work from secure context (https)
		this.props.alert.success("ID da Memória copiado")
	}

	setEditingComment(event){
		this.setState({"editingComment":event.target.value})
	}

	saveNewTags(){
		var payload = {
			"userComment":this.state.editingComment,
			"newTags":this.state.newSelectedTags
		}
		axios.post("/memory/"+this.props.memory._id+"/tags", payload).then((resp)=>{
			if(resp.data.success){
				this.setState({"editingComment":"","isNewTagsOpen":false})
			}
		})
	}

	saveNewSources(){

		var payload = {
			"userComment":this.state.editingComment,
			"newSources": parseSources(this.state.newSources)
		}

		axios.post("/memory/"+this.props.memory._id+"/sources", payload).then((resp)=>{
			if(resp.data.success){
				this.setState({"editingComment":"","isNewSourcesOpen":false})
			}
		})
	}

	saveEditedSources(){

		var payload = {
			"userComment":this.state.editingComment,
			"editedSources": parseSources(this.state.editedSources)
		}

		axios.patch("/memory/"+this.props.memory._id+"/sources", payload).then((resp)=>{
			if(resp.data.success){
				this.setState({"editingComment":"","isEditedSourcesOpen":false})
			}
		})
	}

	mergeMemories(){
		var payload = {
			"userComment":this.state.editingComment,
			"mergedMemory": this.state.mergingMemoryId
		}
		axios.post("/memory/"+this.props.memory._id+"/merge", payload).then((resp)=>{
			if(resp.data.success){
				this.props.history.push("/memoria/"+resp.data.data)
			}
		})
	}

	renderSources(){
		return <ol>{this.props.memory.sources.map((source, index) => {
			return <li key={index}><a href={source.url} target="_blank">{source.headline}</a></li>
		})}</ol>
	}

	renderMenu(memory){
		if (this.props.showEditMenu){
			return <Menu menuButton={
						<button className="memory-menu-btn"><i className="fas fa-ellipsis-v"></i></button>}>
					<MenuItem onClick={this.copyIdToClipboard}>Copiar ID da Memória</MenuItem>
					<MenuItem onClick={()=>{this.setState({isNewSourcesOpen:true})}}>Adicionar Fonte</MenuItem>
					<MenuItem onClick={()=>{this.setState({isEditedSourcesOpen:true})}}>Editar Fontes</MenuItem>
					<MenuItem href={"/memoria/"+memory._id}>Abrir Histórico de Edições</MenuItem>
					<MenuItem onClick={()=>{this.setState({isNewTagsOpen:true})}}>Adicionar Etiqueta(s)</MenuItem>
					<MenuItem onClick={()=>{this.setState({isMergingModalOpen:true})}}>Combinar Notícia Duplicada</MenuItem>
					<MenuItem>Denunciar Notícia Falsa</MenuItem>
				</Menu>
		} else {
			return <div />
		}

	}
	render(){
		var memory = this.props.memory
		var formartedDate = format(parse(memory.publicationDate, 'yyyy-MM-dd HH:mm:ss', new Date()), 'dd/MM/yyyy')

		return (
			<div className="memoria-container">
				<div className="timestamp-col">
					<div className="timestamp">{formartedDate}</div>
				</div>
				<div className="memoria-col">
					<div className="manchete" onClick={()=>{this.setState({"showingSources":!this.state.showingSources})}}>{memory.headline}</div>
					<Collapse isOpened={this.state.showingSources}>{this.renderSources()}</Collapse>
					<TagList tags={memory.tags} />
				</div>

				<div className="menu-memory-col">
					{this.renderMenu(memory)}
				</div>



				<Modal
					isOpen={this.state.isNewTagsOpen}
					onRequestClose={()=>{this.setState({isNewTagsOpen:false})}}
				>
					<TagSelector
						selectedTags = {this.state.newSelectedTags}
						onChange={this.onNewTagsChange}
						/>
					<textarea
						className="editing-comment"
						placeholder="Use este espaço para explicar o motivo desta edição, assim as pessoas que virem o histórico de edições entenderão suas intenções"
						value={this.state.editingComment}
						onChange={this.setEditingComment} />
					<button className="custom-button" onClick={()=>{this.saveNewTags()}}>Adicionar</button>
				</Modal>
				<Modal
					isOpen={this.state.isNewSourcesOpen}
					onRequestClose={()=>{this.setState({isNewSourcesOpen:false})}}
				>
					<SourcesForm
						sources = {this.state.newSources}
						setSources={this.onNewSourcesChange}
						/>
					<textarea
						className="editing-comment"
						placeholder="Use este espaço para explicar o motivo desta edição, assim as pessoas que virem o histórico de edições entenderão suas intenções"
						value={this.state.editingComment}
						onChange={this.setEditingComment} />
					<button className="custom-button" onClick={()=>{this.saveNewSources()}}>Adicionar</button>
				</Modal>
				<Modal
					isOpen={this.state.isEditedSourcesOpen}
					onRequestClose={()=>{this.setState({isEditedSourcesOpen:false})}}
				>
					<SourcesForm
						sources = {this.state.editedSources}
						setSources={(sources) => {this.setState({editedSources:sources})}}
						/>
					<textarea
						className="editing-comment"
						placeholder="Use este espaço para explicar o motivo desta edição, assim as pessoas que virem o histórico de edições entenderão suas intenções"
						value={this.state.editingComment}
						onChange={this.setEditingComment} />
					<button className="custom-button" onClick={()=>{this.saveEditedSources()}}>Salvar Alterações</button>
				</Modal>
				<Modal
					isOpen={this.state.isMergingModalOpen}
					onRequestClose={()=>{this.setState({isMergingModalOpen:false})}}
				>
					<input
						type="text"
						className="custom-txt"
						placeholder="ID da memória duplicada"
						name="mergingMemoryId"
						onChange={this.handleInputChange}
						value={this.state.mergingMemoryId}/>
					<textarea
						className="editing-comment"
						placeholder="Use este espaço para explicar o motivo desta edição, assim as pessoas que virem o histórico de edições entenderão suas intenções"
						value={this.state.editingComment}
						onChange={this.setEditingComment} />
					<button className="custom-button" onClick={()=>{this.mergeMemories()}}>Adicionar</button>
				</Modal>
			</div>
		)
	}
}

export default withRouter(withAlert()(Memory))