import React from "react";
import { Link } from "react-router-dom"; //https://reactrouter.com/web/guides/quick-start
import '../../index.css';
import './Menu.css';
import {axios} from '../../AxiosInstance.js'
import PubSub from 'pubsub-js'

export default class Menu extends React.Component {

	constructor(props){
		super(props)
		this.state = {
			logedin:false
		}
	}

	componentDidMount(){
		axios.get('/login/verify').then( (resp) => {
			this.setState({"logedin":resp.data.success})
		})
		PubSub.subscribe('USER_AUTHENTICATED', (msg, authenticated)=>{
			console.log("USER_AUTHENTICATED", authenticated)
			this.setState({logedin: authenticated})
		});
	}

	accountButton(){
		if(this.state.logedin){
			return <Link to="/conta"> <i className="fas fa-user-alt"></i> Minha conta </Link>
		} else {
			return <Link to="/login"> <i className="fas fa-bookmark"></i> Login</Link>
		}
	}

	render() {
		return (
			<div className="menu-header">
				<div><Link to="/memorias"> <i className="fas fa-bookmark"></i> Memórias</Link></div>
				<div><Link to="/busca"> <i className="fas fa-search"></i> Busca</Link></div>
				<div><Link to="/visualizacao"> <i className="fas fa-project-diagram"></i> Visualização</Link></div>
				<div><Link to="/sobre"> <i className="fas fa-bookmark"></i> Sobre</Link></div>
				<div>{this.accountButton()}</div>
			</div>
		);
	}
}

