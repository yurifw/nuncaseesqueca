import React from 'react';
import '../../index.css';
import './MemoryForm.css';
import {SourcesForm, parseSources} from '../SourcesForm/SourcesForm.js'
import CreatableSelect from 'react-select/creatable'; //https://github.com/JedWatson/react-select
import reactSelectStyles from "../ReactSelectStyles.js"
import FormComponent from '../FormComponent';
import TagSelector from '../TagSelector/TagSelector.js';
import { format } from 'date-fns'
import {axios} from '../../AxiosInstance.js'

export default class MemoryForm extends FormComponent {
	constructor(props){
		super(props)
		this.state = {
			tags: [],
			fontes:[
				{
					headline: "",
					url: "",
					publicationDate: new Date()
				}
			]
		}
		this.updateFontes = this.updateFontes.bind(this)
		this.onTagChange = this.onTagChange.bind(this)
	}
	save(){
		var payload = {
			"tags":this.state.tags,
			"sources":parseSources(this.state.fontes)
		}
		console.log("payload",payload)
		axios.post('/memory',payload).then((resp)=>{
			if(resp.data.success){
				this.setState({
					publicationDate:"",
					fontes:[]
				})
			}
		})

	}

	onTagChange(tags){
		this.setState({"tags":tags})
	}

	updateFontes(fontes){
		this.setState({"fontes":fontes})
	}



	render() {
		return(
			<div className="nova-memoria">

				<SourcesForm setSources={this.updateFontes} sources={this.state.fontes}/>
				<TagSelector onChange={this.onTagChange}	/>

				<button
					className="custom-button"
					onClick={()=>{this.save()}}>
					<i className="fas fa-plus"></i> Salvar</button>
			</div>
		)
	}

}