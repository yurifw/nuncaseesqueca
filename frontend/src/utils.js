import { format, parse } from 'date-fns'

export function formatMongoDateString(date, df){
	df = !df? 'dd/MM/yyyy HH:mm:ss': df
	return format(parse(date, 'yyyy-MM-dd HH:mm:ss', new Date()), df)
}
