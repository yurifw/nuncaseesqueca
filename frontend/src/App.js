import React, { useEffect } from 'react';
import {
	BrowserRouter as Router,
	Switch,
	Route
} from "react-router-dom"; //https://reactrouter.com/web/guides/quick-start
import PubSub from 'pubsub-js'
import { useAlert } from 'react-alert'
import Home from './components/Home/Home.js'
import About from './components/About/About.js'
import MemoryPage from './components/MemoryPage/MemoryPage.js'
import Login from './components/Login/Login.js'
import SignUp from './components/SignUp/SignUp.js'
import Account from './components/Account/Account.js'
import MemoryDetails from './components/MemoryDetails/MemoryDetails.js'
import MemorySearch from './components/MemorySearch/MemorySearch.js'
import UserPage from './components/UserPage/UserPage.js'
import VisualizationGraph from './components/VisualizationGraph/VisualizationGraph.js'
import VisualizationPage from './components/VisualizationPage/VisualizationPage.js'
import ScrapedMemoryDisplay from './components/ScrapedMemoriesDisplay/ScrapedMemoriesDisplay.js'

export default function App() {
	const alert = useAlert()

	var responseNotifier = function (msg, data) {
		if(data.msg){
			alert.show(data.msg)
		}
	};
	useEffect(()=>{
		var token = PubSub.subscribe('RESPONSE_INTERCEPTED', responseNotifier);
	})

	return (
		<Router>
			<Switch>
				<Route path="/home"><Home /></Route>
				<Route path="/sobre"><About /></Route>
				<Route path="/memorias"><MemoryPage /></Route>
				<Route path="/login"><Login /></Route>
				<Route path="/cadastro"><SignUp /></Route>
				<Route path="/conta"><Account /></Route>
				<Route path="/busca"><MemorySearch /></Route>
				<Route path="/memoria/:id" render={(props) => <MemoryDetails {...props} />}></Route>
				<Route path="/usuario/:username" render={(props) => <UserPage {...props} />}></Route>
				<Route path="/visualizacao"><VisualizationPage /></Route>
				<Route path="/bot"><ScrapedMemoryDisplay /></Route>
				<Route path="/"><Home /></Route>
			</Switch>
		</Router>
	);
}