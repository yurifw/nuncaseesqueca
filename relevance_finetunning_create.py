# https://platform.openai.com/docs/guides/fine-tuning
# https://platform.openai.com/docs/guides/fine-tuning/preparing-your-dataset
import pymongo
import os
import json
import pymongo
import openai
import codecs
import random

openai.api_key = os.environ['OPENAI_KEY']



def classify():
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).nuncaseesqueca
	
	memories = db.scrapedMemories.aggregate([
		{"$lookup": {
			"from": "fineTunningRelevance",
			"localField": "_id",
			"foreignField": "memory_id",
			"as": "result"
		}},
		{"$match": {"$expr":{"$lte": [{ "$size": "$result" }, 0]}}}
	])
	memories = list(memories)
	random.shuffle(memories)
	total_categorized = 0

	for memory in memories:
		del memory['result']
		os.system('cls')
		print(f"total categorized: {total_categorized}")
		print(memory['headline'])
		relevant = ""
		while relevant not in ['0', '1']:
			relevant = input("\nconfirm relevance: 1 for relevant, 0 for irrelevant\n")
		relevant = int(relevant) == 1
		memory['is_relevant'] = relevant
		db.scrapedMemories.update_one({'_id': memory['_id']}, {'$set': {'is_relevant': relevant}})

		memory['memory_id'] = memory['_id']
		db.fineTunningRelevance.insert_one(memory)
		total_categorized += 1

def create_finetunning():
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).nuncaseesqueca
	output_file = 'relevance_finetunning.jsonl'
	memories = db.fineTunningRelevance.find({})
	file_content = []

	for memory in memories:
		text = f"{memory['headline']}\n\n###\n\n"
		completion = " 1---" if memory['is_relevant'] else " 0---"
		file_content.append({
			"prompt": text,
			"completion": completion
		})
	
	with codecs.open(output_file, 'w', encoding='utf-8') as f:
		for line in file_content:
			f.write(json.dumps(line, ensure_ascii=False) + '\n')
	file_path = input(f"file generated: {output_file}, you can validate the file with 'openai tools fine_tunes.prepare_data -f <file_name>' after you validate it paste the new file name here\n")
	
	upload_response = openai.File.create(
		file=open(file_path, "rb"),
		purpose='fine-tune'
	)
	print("file uploaded, response:")
	print(upload_response)

	file_id = upload_response['id']

	r = openai.FineTune.create(training_file=file_id, model="ada")
	print("fine tuning created:")
	print(r)


def test_model():
	headline = "'Ditadura nunca mais', diz Gilmar Mendes após general comentar declaração de Edson Fachin"
	prompt = f"{headline}\n\n###\n\n",
	fine_tuned_model = "ada:ft-personal-2023-07-04-15-29-03"
	answer = openai.Completion.create(
		model=fine_tuned_model,
		prompt=prompt,
		temperature=0,
		stop=["---"]
	)
	answer = answer['choices'][0]['text']
	answer = answer.strip()
	answer = answer == "1"
	print(answer)


if __name__ == '__main__':
	test_model()