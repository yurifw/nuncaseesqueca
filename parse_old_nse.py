import scrapers.folha as folha
import scrapers.g1 as g1
import pymongo
import os 
import time



def move_memories_to_new_memories():
	"""Scrapes the news text, and formats the memories to the new format then moves all memories from the old_nse.memories collection to the old_nse.newMemories collection. """
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).old_nse

	memories = list(db.memories.find({}))
	for memory in memories:
		urls = [s['url'] for s in memory['versions'][-1]['sources']]
		for url in urls:
			print("\nscraping %s" % url)
			new_memory = {}
			if 'g1' in url:
				new_memory = g1.scrape_url(url)
			elif 'folha' in url:
				new_memory = folha.scrape_url(url)
			else:
				print("URL not recognized: %s" % url)
				continue
			if new_memory is None:
				print("no new memory")
				continue
			print("saving new memory")
			new_memory['tags'] = memory['versions'][-1]['tags']
			db.newMemories.insert_one(memory)
			time.sleep(1)

def set_is_relevant_field():
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).old_nse
	db.newMemories.update_many({}, {'$set': {'is_relevant': True}})
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).nuncaseesqueca
	db.scrapedMemories.update_many({}, {'$set': {'is_relevant': None}})
	db.fineTunningData.update_many({}, {'$set': {'is_relevant': None}})
	db.fineTunningData.update_many({'tags':['irrelevante']}, {'$set': {'is_relevant': False}})
	db.fineTunningData.update_many({'is_relevant': None}, {'$set': {'is_relevant': True}})

if __name__ == '__main__':
	set_is_relevant_field()