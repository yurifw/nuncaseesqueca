import os
import pymongo

if __name__ == '__main__':
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).nuncaseesqueca
	db.scrapedMemories.create_index([('url', pymongo.ASCENDING)], unique=True)
	db.memories.create_index([('url', pymongo.ASCENDING)], unique=True)
	db.fineTunningData.create_index([('memory_id', pymongo.ASCENDING)], unique=True)
	db.fineTunningRelevance.create_index([('memory_id', pymongo.ASCENDING)], unique=True)