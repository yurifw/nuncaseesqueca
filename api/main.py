# nohup gunicorn -w 4 --reload main:app > /root/mgc-scrapers/gunicorn.log &

import os
from flask import Flask, send_file, make_response
from flask import render_template
from flask_cors import CORS
from flask import request
from nseutils.MongoJsonEncoder import MongoJsonEncoder

import endpoints.user as user
import endpoints.auth as auth
import endpoints.tag as tag
import endpoints.memory as memory
import endpoints.scraped_memories as scraped_memories

app = Flask(__name__, static_folder='public/', template_folder='public/')
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
app.debug = True
app.json_encoder = MongoJsonEncoder
CORS(app)

def test_api():
	return make_response({"msg":"API is up", "success":True, "data":None})

app.add_url_rule('/api/test', 't', test_api, methods=['GET'])
app.add_url_rule('/api/user/all', 'ul', user.list, methods=['GET'])
app.add_url_rule('/api/user', 'ud', user.details, methods=['GET'])
app.add_url_rule('/api/user/<username>', 'unp', user.user_profile, methods=['GET'])
app.add_url_rule('/api/user', 'un', user.update, methods=['PATCH'])
app.add_url_rule('/api/user', 'up', user.new, methods=['POST'])
app.add_url_rule('/api/tag', 'tl', tag.list_all, methods=['GET'])
app.add_url_rule('/api/tag/replacement', 'tnr', tag.new_replacement, methods=['POST'])
app.add_url_rule('/api/tag/replacement', 'tlr', tag.list_replacements, methods=['GET'])
app.add_url_rule('/api/tag/replacement/<id>', 'trr', tag.remove_replacement, methods=['DELETE'])
app.add_url_rule('/api/login', 'al', auth.login, methods=['POST'])
app.add_url_rule('/api/login/verify', 'av', auth.verify_token, methods=['GET'])
app.add_url_rule('/api/login/refresh', 'ar', auth.refresh_token, methods=['GET'])
app.add_url_rule('/api/memory', 'mb', memory.new, methods=['POST'])
app.add_url_rule('/api/memory', 'ml', memory.list, methods=['GET'])
app.add_url_rule('/api/memory/scraped', 'ms', scraped_memories.scraped_memories, methods=['GET'])
app.add_url_rule('/api/memory/scraped/<scraped_id>/accept', 'sma', scraped_memories.convert_to_memory, methods=['POST'])
app.add_url_rule('/api/memory/scraped/<scraped_id>/discard', 'smd', scraped_memories.discard, methods=['POST'])
app.add_url_rule('/api/memory/scraped/<scraped_id>/merge/<memory_id>', 'smm', scraped_memories.add_as_source, methods=['POST'])
app.add_url_rule('/api/memory/<memory_id>/tags', 'mat', memory.add_tags, methods=['POST'])
app.add_url_rule('/api/memory/<memory_id>/sources', 'mas', memory.add_sources, methods=['POST'])
app.add_url_rule('/api/memory/<memory_id>/sources', 'mes', memory.edit_sources, methods=['PATCH'])
app.add_url_rule('/api/memory/<memory_id>/merge', 'mmm', memory.merge_memory, methods=['POST'])
app.add_url_rule('/api/memory/<memory_id>/versions', 'mlv', memory.list_versions, methods=['GET'])

@app.errorhandler(404)
def not_found(e):
	requested_file = request.path[1:]
	if len(request.path[1:]) == 0:
		return app.send_static_file("index.html")

	if '.' in requested_file:
		requested_file = '/'.join(request.path.split("/")[1:])
		return app.send_static_file(requested_file)
	else:
		print("serving index")
		return app.send_static_file("index.html")
