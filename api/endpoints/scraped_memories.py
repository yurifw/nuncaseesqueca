from flask import Flask, make_response
from flask import request
from datetime import datetime
from nseutils.utils import get_db
from .auth import reputation_required, _decode_token
from .memory import _create_memory, _add_sources
from bson import ObjectId
import copy

# @reputation_required(200)
def scraped_memories():
	db = get_db()
	args = request.args
	offset = int(args.get('offset','0'))
	limit = int(args.get('limit','10'))
	sort = args.get('sort','new')
	sort = -1 if sort == 'new' else 1
	memories = db.scrapedMemories.aggregate([
		{"$match" : {"status":"waiting"}},
		{"$sort": {"sources.publicationDate":sort, "url":1}},
		{"$skip": offset},
		{"$limit": limit}
	])

	tagReplacements = db.tagReplacements.find({})
	replacements = {}
	for r in tagReplacements:
		replacements[r['replaced']] = r['replacer']
	replaced_tags = []
	memories = [m for m in memories]

	for memory in memories:
		memory['tags'] = map(lambda t:replacements.get(t, t), memory['tags'])
		memory['tags'] = list(memory['tags'])
	return make_response({"msg":None,"success":True, "data":memories})

@reputation_required(200)
def convert_to_memory(scraped_id):
	user= _decode_token(request.headers['Authorization'])['payload']['username']
	db = get_db()
	scraped = db.scrapedMemories.find_one({"_id":ObjectId(scraped_id)},{"_id":0,"sources":1,"tags":1})
	version = copy.deepcopy(scraped)
	version['sources'][0]['publicationDate'] = version['sources'][0]['publicationDate'].strftime('%Y-%m-%d %H:%M')
	version['tags'] = request.json['tags']

	for source in version['sources']:
		url_occurrences = db.memories.count({"versions.sources.url":source['url']})
		if url_occurrences > 0:
			return make_response({"msg":"Esta memória já está cadastrada", "success":False, "data":None})

	id = None
	try:
		id = _create_memory(user, version, db)
		db.scrapedMemories.update_one({"_id":ObjectId(scraped_id)}, {"$set":{"status":"accepted"}})
		return make_response({"msg":"Memória salva!", "success":True, "data":None})
	except ValueError as e:
		return make_response({"msg":str(e), "success":False, "data":None})

@reputation_required(200)
def add_as_source(scraped_id, memory_id):
	user= _decode_token(request.headers['Authorization'])['payload']['username']
	db = get_db()

	scraped = db.scrapedMemories.find_one({"_id":ObjectId(scraped_id)},{"_id":0,"sources":1})
	scraped['sources'][0]['publicationDate'] = scraped['sources'][0]['publicationDate'].strftime('%Y-%m-%d %H:%M')
	_add_sources(
		user,
		memory_id,
		scraped['sources'],
		"",
		db
	)
	db.scrapedMemories.update_one({"_id":ObjectId(scraped_id)}, {"$set":{"status":"merged"}})
	return make_response({"msg":"Fonte Adicionada","success":True, "data":None})



@reputation_required(200)
def discard(scraped_id):
	db = get_db()
	db.scrapedMemories.update_one({"_id":ObjectId(scraped_id)}, {"$set":{"status":"discarded"}})
	return make_response({"msg":"Memória Descartada","success":True, "data":None})