from flask import Flask, make_response
from flask import request
from nseutils.utils import get_db
from bson import ObjectId

def list_all():
	db = get_db()
	tags = db.tags.find({}).sort([("value",1)])
	tags = [tag['value'] for tag in tags]
	return make_response({"msg":None, "success":True, "data":tags})

def new_replacement():
	db = get_db()
	db.tagReplacements.insert_one(request.json)
	return make_response({"msg":"Substituição adicionada!", "success":True, "data":None})

def list_replacements():
	db = get_db()
	tags = db.tagReplacements.find({}).sort([("replaced",1)])
	tags = [t for t in tags]
	return make_response({"msg":None, "success":True, "data":tags})

def remove_replacement(id):
	db = get_db()
	db.tagReplacements.delete_one({"_id":ObjectId(id)})
	return make_response({"msg":"Subsituição removida", "success":True, "data":None})