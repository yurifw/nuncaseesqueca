from flask import Flask, make_response
from flask import request
from datetime import datetime
from nseutils.utils import get_db
from .auth import reputation_required, _decode_token
from pymongo.errors import DuplicateKeyError
from bson import ObjectId
from . import actions
import copy

def _load_memory_collections(memories, db):
	unwinded_versions = []
	for memory in memories:
		unwinded = memory.copy()
		unwinded.update(memory['versions'][-1])
		unwinded['headline'] = unwinded['sources'][0]['headline']
		unwinded['publicationDate'] = unwinded['firstPublication']
		del unwinded['versions']
		del unwinded['lastVersion']
		del unwinded['firstPublication']
		unwinded_versions.append(unwinded)
	return unwinded_versions

def _add_tag_suggestion(new_tag, db):
	try:
		db.tags.insert_one({"value":new_tag})
	except DuplicateKeyError:
		pass

def _create_memory(username, version, db):
	memory = {
		"createdBy": username,
		"creationDate": datetime.now(),
		"status": "active"
	}
	version['modifiedBy'] = username
	version['modificationDate'] = memory['creationDate']
	version['userComment'] = None
	version['systemComment'] = "Primeira versão da memória"
	if len(version['tags']) < 1:
		raise ValueError("Adicione pelo menos uma etiqueta")
	if len(version['sources']) < 1:
		raise ValueError("Adicione pelo menos uma fonte")

	for source in version['sources']:
		source['publicationDate'] = datetime.strptime(source['publicationDate'], '%Y-%m-%d %H:%M')

	if len(version['tags']) != len(set(version['tags'])):
		raise ValueError("Por favor remova as tags repetidas")

	for tag in version['tags']:
		_add_tag_suggestion(tag, db)

	memory['versions'] = [version]
	id = db.memories.insert_one(memory).inserted_id
	actions._new_memory(username, ObjectId(memory['_id']),db)
	return id

def _add_sources(username, memory_id, sources, coments, db):
	memory = db.memories.find_one({"_id":ObjectId(memory_id)})
	version = memory['versions'][-1]
	version['modificationDate'] = datetime.now()
	version['modifiedBy'] = username
	version['userComment'] = coments
	version['systemComment'] = "Fonte(s) adicionada(s)"
	new_sources = []
	for source in sources:
		source['publicationDate'] = datetime.strptime(source['publicationDate'], '%Y-%m-%d %H:%M')
		new_sources.append(source)
	version['sources'].extend(new_sources)
	db.memories.update({"_id":ObjectId(memory_id)}, {"$push":{"versions":version}})
	actions._new_sources(username, ObjectId(memory_id), new_sources, db)

@reputation_required(0)
def new():
	decoded = _decode_token(request.headers['Authorization'])['payload']
	db = get_db()
	version = request.json
	for source in version['sources']:
		url_occurrences = db.memories.count({"versions.sources.url":source['url']})
		if url_occurrences > 0:
			return make_response({"msg":"Esta memória já está cadastrada", "success":False, "data":None})

	try:
		_create_memory(decoded['username'], version, db)
		return make_response({"msg":"Memória salva!", "success":True, "data":None})
	except ValueError as e:
		return make_response({"msg":str(e), "success":False, "data":None})

def list():
	# https://stackoverflow.com/questions/60564623/is-there-a-way-around-mongo-lookup-from-overwritten-subdocument
	# https://stackoverflow.com/questions/36022456/mongodb-lookup-on-nested-document/36023726
	try:
		db = get_db()
		args = request.args
		tags_all = args.get('tags_all',"")
		status = args.get('status','active')
		offset = int(args.get('offset','0'))
		limit = int(args.get('limit','10'))
		sort = args.get('sort','new')  # by default show new publications first

		match = {"status":status}
		aggregation = [
			{"$addFields": {"lastVersion":{"$last": "$versions"}}},
			{"$addFields": {
				"firstPublication":{"$min":"$lastVersion.sources.publicationDate"}
			}},
			{"$match": match}
		]
		if len(tags_all) > 0:
			tags_all = tags_all.split(",")
			match["lastVersion.tags"] = {"$all": tags_all}

		sort = -1 if sort == 'new' else 1

		aggregation.append({"$sort": {"firstPublication":sort, "_id":1}})
		aggregation.append({"$skip": offset})
		aggregation.append({"$limit": limit})
		result = db.memories.aggregate(aggregation)
		m = _load_memory_collections(result, db)
		return make_response({"msg":None, "success":True, "data":m})
	except ValueError as ve:
		return make_response({"msg":str(ve), "success":False, "data":None})

@reputation_required(0)
def add_tags(memory_id):
	decoded = _decode_token(request.headers['Authorization'])['payload']
	db = get_db()
	memory = db.memories.find_one({"_id":ObjectId(memory_id)})
	version = memory['versions'][-1]
	version['modificationDate'] = datetime.now()
	version['modifiedBy'] = decoded['username']
	version['userComment'] = request.json['userComment']
	version['systemComment'] = "Etiquetas adicionadas"
	new_tags = request.json['newTags']
	version['tags'].extend(new_tags)
	if len(version['tags']) != len(set(version['tags'])):
		return make_response({"msg":"Por favor remova as tags repetidas", "success":False, "data":None})
	db.memories.update({"_id":ObjectId(memory_id)}, {"$push":{"versions":version}})
	actions._new_tags(decoded['username'], ObjectId(memory_id), new_tags, db)
	return make_response({"msg":"Etiquetas adicionadas", "success":True, "data":None})

def list_versions(memory_id):
	db = get_db()
	memory = db.memories.find_one({"_id":ObjectId(memory_id)})
	return make_response({"msg":None, "success":True, "data":memory})

@reputation_required(0)
def add_sources(memory_id):
	decoded = _decode_token(request.headers['Authorization'])['payload']
	db = get_db()
	_add_sources(
		decoded['username'],
		memory_id,
		request.json['newSources'],
		request.json['userComment'],
		db
	)

	return make_response({"msg":"Fontes adicionadas", "success":True, "data":None})

@reputation_required(0)
def edit_sources(memory_id):
	username = _decode_token(request.headers['Authorization'])['payload']['username']
	db = get_db()
	memory = db.memories.find_one({"_id":ObjectId(memory_id)})
	version = copy.deepcopy(memory['versions'][-1])
	version['modificationDate'] = datetime.now()
	version['modifiedBy'] = username
	version['userComment'] = request.json['userComment']
	version['systemComment'] = "Fonte(s) editada(s)"
	version['sources'] = request.json['editedSources']
	for source in version['sources']:
		source['publicationDate'] = datetime.strptime(source['publicationDate'], '%Y-%m-%d %H:%M')
	db.memories.update({"_id":ObjectId(memory_id)}, {"$push":{"versions":version}})
	actions._edited_sources(username, ObjectId(memory_id), memory['versions'][-1]['sources'], version['sources'], db)
	return make_response({"msg":"Fontes editadas", "success":True, "data":None})

@reputation_required(0)
def merge_memory(memory_id):
	decoded = _decode_token(request.headers['Authorization'])['payload']
	db = get_db()
	memory_a = db.memories.find_one({"_id":ObjectId(memory_id)})
	memory_b = db.memories.find_one({"_id":ObjectId(request.json['mergedMemory'])})
	new_memory = copy.deepcopy(memory_a)
	del new_memory['_id']
	new_memory['createdBy'] = decoded['username']
	new_memory['creationDate'] = datetime.now()
	new_memory['mergedMemories'] = [memory_a['_id'], memory_b['_id']]
	new_memory['versions'] = [memory_a['versions'][-1]]
	for tag in memory_b['versions'][-1]['tags']:
		if tag not in new_memory['versions'][-1]['tags']:
			new_memory['versions'][-1]['tags'].append(tag)
	for source in memory_b['versions'][-1]['sources']:
		if source not in new_memory['versions'][-1]['sources']:
			new_memory['versions'][-1]['sources'].append(source)
	new_memory['versions'][-1]['modifiedBy'] = decoded['username']
	new_memory['versions'][-1]['modificationDate'] = new_memory['creationDate']
	new_memory['versions'][-1]['userComment'] = request.json['userComment']
	new_memory['versions'][-1]['systemComment'] = "Resultado da combinação das duplicatas"
	new_id = db.memories.insert_one(new_memory).inserted_id
	db.memories.update_one({"_id":memory_a['_id']}, {"$set":{'status':'merged','mergedInto':new_id}})
	db.memories.update_one({"_id":memory_b['_id']}, {"$set":{'status':'merged','mergedInto':new_id}})
	actions._merge_memories(decoded['username'], memory_a['_id'], memory_b['_id'], new_id, db)
	return make_response({"msg":"Memórias combinadas!", "success":True, "data":new_id})

