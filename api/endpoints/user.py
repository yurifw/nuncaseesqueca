from flask import Flask, make_response
from flask import request
from nseutils.utils import get_db
from .auth import _generate_token, reputation_required, _decode_token
from pymongo.errors import DuplicateKeyError
import jwt
from jwt.exceptions import ExpiredSignatureError
import bcrypt
from bson.json_util import dumps


project_user = {
	"$project": {
		"_id":0,
		"actions":1,
		"username":1,
		"email":1,
		"reputation":{"$sum":"$actions.modifier"}
	}
}


@reputation_required(0)
def details():
	db = get_db()
	decoded = _decode_token(request.headers['Authorization'])['payload']
	user = db.users.aggregate([
		{"$match":{"username":decoded['username']}},
		project_user
	])
	user = [u for u in user]
	return make_response({"msg":None, "success":True, "data":user[0]})

def user_profile(username):
	db = get_db()
	user = db.users.find_one({"username":username}, project_user['$project'])
	return make_response({"msg":None, "success":True, "data":user})

def new():
	db = get_db()
	user = request.json
	hashed = bcrypt.hashpw(user['password'].encode(), bcrypt.gensalt())
	user['password'] = hashed.decode()
	user['email'] = user.get('email',None)
	user['actions'] = []

	try:
		new = db.users.insert_one(user)
		jwt = _generate_token(user['username'])
		return make_response({"msg":"Cadastro realizado!", "success":True, "data":jwt})
	except DuplicateKeyError:
		return make_response({"msg":"Nome de usuário em uso, por favor escolha um novo", "success":False, "data":None})


def list():
	db = get_db()
	users = db.users.aggregate([project_user])
	users = [user for user in users]
	return make_response({"msg":None, "success":True, "data":users})

@reputation_required(0)
def update():
	db = get_db()
	decoded = _decode_token(request.headers['Authorization'])['payload']
	new = request.json
	update= {"$set":{"email":new['email']}}
	if len(new['newPassword'])>0:
		saved = db.users.find({"username":decoded['username']})
		saved = [u for u in saved][0]
		if bcrypt.checkpw(new['oldPassword'].encode(), saved['password'].encode()):
			hashed = bcrypt.hashpw(new['newPassword'].encode(), bcrypt.gensalt())
			update["$set"]["password"] = hashed.decode()
		else:
			return make_response({"msg":"Senha inválida!", "success":False, "data":None})

	db.users.update({"username":decoded['username']},update)
	return make_response({"msg":"Dados atualizados!", "success":True, "data":None})

