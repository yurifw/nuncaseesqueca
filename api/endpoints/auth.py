from flask import Flask, make_response
from flask import request
from datetime import datetime, timedelta
import jwt
import bcrypt
from functools import wraps
from pymongo.errors import DuplicateKeyError
from jwt.exceptions import ExpiredSignatureError
from nseutils.utils import get_db
# from nseutils.utils import calculate_reputation
from endpoints import JWT_SECRET
import endpoints.user as user_helper

def reputation_required(rep): #meant to be used as decorator
	def decorator(func):
		@wraps(func)
		def decorated_func(*args, **kws):
			token = None
			try:
				token = request.headers['Authorization'].replace("Bearer ","")
			except KeyError:
				return {"msg":"Reputação mínima necessária: %s" % rep, "data":None, "success":False}
			decoded = _decode_token(token)
			if decoded['isValid'] and decoded['payload']['reputation'] >= rep:
				return func(*args, **kws)
			else:
				return {"msg":"Reputação mínima necessária: %s" % rep, "data":None, "success":False}
		return decorated_func
	return decorator

def _generate_token(username, reputation=0, quick_expire = False):
	if quick_expire:
		expiration = datetime.utcnow() + timedelta(minutes=15)
	else:
		expiration = datetime.utcnow() + timedelta(days=30)

	payload = {
		"username": username,
		"exp":expiration,
		"reputation":reputation
	}
	token = jwt.encode(payload, JWT_SECRET, algorithm='HS512')
	return token

def _decode_token(token):
	valid = False
	payload =None
	try:
		token = token.replace("Bearer ","")
		payload = jwt.decode(token, JWT_SECRET, algorithms=['HS512'], verify=True)
		valid = True
	except Exception as e:

		pass

	decoded_token = {"isValid":valid, "payload":payload}
	return decoded_token

def verify_token():
	decoded = _decode_token(request.headers.get('Authorization'))
	return make_response({"msg":None, "success":decoded['isValid'], "data":decoded['payload']})

@reputation_required(0)
def refresh_token():
	db = get_db()
	decoded = _decode_token(request.headers['Authorization'])['payload']
	username = decoded['username']
	user = db.users.find_one(
		{"username":username},
		{"_id":0, "actions":1,"reputation":{"$sum":"$actions.modifier"}}
	)
	jwt = _generate_token(username, user['reputation'])
	return make_response({"msg":None, "success":True, "data":jwt})

def login():
	db = get_db()
	user = request.json

	try:
		saved = db.users.aggregate([
			{"$match":{"username":user['username']}},
			{"$project": {"_id":0, "actions":1, "username":1, "password":1,"reputation":{"$sum":"$actions.modifier"}}}
		])
		saved = [u for u in saved][0]
	except IndexError:
		return make_response({"msg":"Usuário não encontrado", "success":False, "data":None})

	jwt = None
	msg = "Senha inválida"
	if bcrypt.checkpw(user['password'].encode(), saved['password'].encode()):
		jwt = _generate_token(saved['username'], saved['reputation'])
		msg = "Login realizado com sucesso"

	return make_response({"msg":msg, "success":jwt is not None, "data":jwt})