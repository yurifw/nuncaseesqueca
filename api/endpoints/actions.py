from datetime import datetime

def _new_memory(username, memory_id, db):
	action = {
		"code":"NEWMEMORY",
		"modifier": 20,
		"description": "Memória Cadastrada",
		"date": datetime.now(),
		"meta":{
			"targetMemory": memory_id
		}
	}
	db.users.update({"username":username}, {"$push":{"actions":action}})

def _new_tags(username, memory_id, tags, db):
	action = {
		"code":"NEWTAG",
		"modifier": 5,
		"description": "Etiqueta(s) adicionada(s)",
		"date": datetime.now(),
		"meta":{
			"targetMemory": memory_id,
			"tagsAdded":tags
		}
	}
	db.users.update({"username":username}, {"$push":{"actions":action}})

def _new_sources(username, memory_id, sources, db):
	action = {
		"code":"NEWSOURCES",
		"modifier": 25,
		"description": "Fonte(s) adicionada(s)",
		"date": datetime.now(),
		"meta":{
			"targetMemory": memory_id,
			"sourcesAdded":sources
		}
	}
	db.users.update({"username":username}, {"$push":{"actions":action}})

def _edited_sources(username, memory_id, old_sources, new_sources, db):
	action = {
		"code":"NEWSOURCES",
		"modifier": 25,
		"description": "Fonte(s) editadas(s)",
		"date": datetime.now(),
		"meta":{
			"targetMemory": memory_id,
			"oldSources": old_sources,
			"newSources": new_sources
		}
	}
	db.users.update({"username":username}, {"$push":{"actions":action}})

def _merge_memories(username, memory_a_id, memory_b_id, resulting_id, db):
	action = {
		"code":"MERGEDMEMORIES",
		"modifier": 20,
		"description": "Memórias Combinadas",
		"date": datetime.now(),
		"meta":{
			"firstMemory":memory_a_id,
			"secondMemory": memory_b_id,
			"resultingMemory": resulting_id
		}
	}
	db.users.update({"username":username}, {"$push":{"actions":action}})


