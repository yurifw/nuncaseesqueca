import boto3
import os
import shutil

ZIP_DIR = 'package'
PIP_PLATFORM = 'manylinux2014_x86_64'

def upload_to_lambda(function_name):
	client = boto3.client('lambda',
		region_name= os.environ['AWS_LAMBDA_REGION'],
		aws_access_key_id= os.environ['AWS_LAMBDA_KEY_ID'],
		aws_secret_access_key= os.environ['AWS_LAMBDA_SECRET_KEY'])


	client.update_function_code(
		FunctionName=function_name,
		ZipFile=open(f'./{ZIP_DIR}.zip', 'rb').read()
	)

def zip_code(package_path):
	
	deploy_files = []
	for file in os.listdir(package_path):
		if file.endswith(".py"):
			deploy_files.append(os.path.join(package_path, file))

	try:
		os.mkdir(ZIP_DIR) 
	except OSError as error:
		shutil.rmtree(ZIP_DIR, ignore_errors=True)
		os.mkdir(ZIP_DIR)
	
	for file in deploy_files:
		shutil.copy(file, os.path.join(ZIP_DIR, file.split(os.sep)[-1]))

	requirements = os.path.join(package_path, 'requirements.txt')
	install_command = f"python -m pip install -r {requirements} --target ./{ZIP_DIR} --only-binary=:all: --platform {PIP_PLATFORM}"
	os.system(install_command)
	shutil.make_archive(ZIP_DIR, 'zip', ZIP_DIR)


def clear():
	files_2_clear = [ZIP_DIR, f"{ZIP_DIR}.zip"]
	for file in files_2_clear:
		try:
			if os.path.isdir(file):
				shutil.rmtree(file, ignore_errors=True)
			else:
				os.remove(file)
		except FileNotFoundError as error:
			print(f"file {file} not found")
		except OSError as error:
			print(f"file {file} not found")

def deploy_scraper():
	print(f"packaging daemon code and dependencies for {os.environ['AWS_LAMBDA_FUNCTION']}")
	zip_code('scrapers')
	print(f"updating {os.environ['AWS_LAMBDA_FUNCTION']} lambda function")
	upload_to_lambda(os.environ['AWS_LAMBDA_FUNCTION'])
	print("cleaning up\n")
	clear()

if __name__ == '__main__':
	deploy_scraper()