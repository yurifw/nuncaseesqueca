import requests
import re
from datetime import datetime, timezone
from lxml import html
import traceback

def get_urls():
	response = requests.get("https://www.folha.uol.com.br/")
	page_text = response.text
	tree = html.fromstring(page_text)
	anchors = tree.xpath("//a[contains(@class, 'headline')]")
	urls = []
	for a in anchors:
		url = a.get("href")

		urls.append(url)

	return urls

def replace_month(date_string):
	replacements = {
		"jan": "01",
		"fev": "02",
		"mar": "03",
		"abr": "04",
		"mai": "05",
		"jun": "06",
		"jul": "07",
		"ago": "08",
		"set": "09",
		"out": "10",
		"nov": "11",
		"dez": "12"
	}
	for key in replacements:
		date_string = date_string.replace(key, replacements[key])
	return date_string

def get_xpath_selectors(url):
	default = {
		"date":"//time[contains(@class, 'published')]",
		"headline":"//h1[contains(@class, 'c-content-head__title')]",
		"text": "//div[@class='c-news__body']",
		"date_format":"%d.%m.%Y às %Hh%M",
		"to_clear": ["//div[contains(@class, 'gallery-widget')]"]
	}
	if 'mensageirosideral.blogfolha' in url:
		return {
			"date":"//div[@class='c-content-head__wrap']",
			"headline":"//h2[@class='c-content-head__title']",
			"text": "//div[@class='c-news']",
			"date_format":"%d.%m.%Y às %Hh%M",
			"to_clear": []
		}
	if 'piaui.folha' in url:
		return {
			"date":"//span[@class='noticia__header--autor--data']",
			"headline":"//h1[@class='noticia__header--title']",
			"text": "//div[@class='contentpaywall']",
			"date_format":"%d%m%Y_%Hh%M",
			"to_clear": []
		}
	return default

def clear_html_content(tree, selectors):
	for xpath in selectors:
		for element in tree.xpath(xpath):
			element.getparent().remove(element)
	return tree.text_content().strip()

def clear_text_content(text):
	text = re.sub(r'\s{2,}', '\n', text)
	text = re.sub(r'\n{2,}', '\n', text)
	return text

def scrape_url(url):
	try:
		result = {'headline':'', 'text':'', 'url':'', 'publication_date':'', 'tags':[], 'publisher':'Folha de São Paulo', 'scraped_date':datetime.now()}
		response = requests.get(url)
		page_text = response.content
		selectors = get_xpath_selectors(url)
		tree = html.fromstring(page_text)
		date = tree.xpath(selectors['date'])[0].text_content()
		date = replace_month(date).strip().replace("º","")
		try:
			date = datetime.strptime(date, selectors['date_format'])
		except ValueError:
			date = datetime.strptime(date, "%d.%m.%Y à %Hh%M")
		headline = tree.xpath(selectors['headline'])[0].text_content().strip()
		
		text = tree.xpath(selectors['text'])[0]
		text = clear_html_content(text, selectors['to_clear'])

		result['headline'] = headline
		result['text'] = clear_text_content(text)
		result['url'] = url
		result['publication_date'] = date
		result['is_relevant'] = None
		return result
	except:
		traceback.print_exc()
		print("Faulty URL: %s" % url)
		return None

def should_skip_url(url):
	skip_words = [
	 	'/webstories/', '/celebridades/', '/restaurantes/', '/cinema-e-series/', '/turismo/',
		'/passeios/', '/ilustrada/', 'copocheio.blogfolha', 'f5.folha.', 'arte.folha', 'clube.folha',
		'aovivo.folha', '.blogfolha.', 'minhafolha.', 'login.', 'guia.', 'fotografia.folha', 'folha.uol.com.br/opiniao/',
		'folha.uol.com.br/cotidiano', 'folha.uol.com.br/comida/', '/campeonato-brasileiro/'
	]
	for word in skip_words:
		if word in url or len(url)<1:
			return True
	return False

def scrape_news():
	urls = get_urls()
	news = []
	for url in urls:
		if should_skip_url(url):
			continue
		r = scrape_url(url)
		if r is not None:
			news.append(r)

	print("Folha de Sao Paulo scraped.")	
	return news

if __name__ == '__main__':
	url = "https://www1.folha.uol.com.br/esporte/campeonato-brasileiro/tabela-e-classificacao/"
	r = scrape_url(url)
	print(r['text'])
