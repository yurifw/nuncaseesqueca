import pymongo
import openai
import os
import g1
import folha
import bbc_brasil  # in the fridge for now, too many repeated news with g1
from pymongo.write_concern import WriteConcern

openai.api_key = os.environ['OPENAI_KEY']


def classify_relevance(headline):
	"""Classifies the relevance of a headline using OpenAI's GPT-3. Returns a boolean."""
	prompt =f"{headline}\n\n###\n\n"
	completion = openai.Completion.create(model=os.environ['RELEVANCE_MODEL'], prompt=prompt, temperature=0, stop=["---"])
	answer = completion.choices[0].text
	answer = answer.strip()
	return answer == "1"
	

def main():
	db = pymongo.MongoClient(os.environ['MONGO_STRING']).nuncaseesqueca
	scraped_urls = [x['url'] for x in db.scrapedMemories.find({}, {'url': 1})]
	news = []

	g1_news = g1.scrape_news()
	news.extend(g1_news)
	folha_news = folha.scrape_news()
	news.extend(folha_news)

	print("all scrapers finished")

	duplicated = 0
	for n in news:
		if n['url'] in scraped_urls:
			duplicated += 1
			continue
		try:
			n['is_relevant'] = classify_relevance(n['headline'])
			db.scrapedMemories.insert_one(n)
		except pymongo.errors.DuplicateKeyError:
			duplicated += 1
	print(f"G1: {len(g1_news)}, Folha: {len(folha_news)}")
	print(f"{len(news)} memories scraped, {duplicated} duplicated.")
	print("run successfull")

if __name__ == "__main__":
	main()