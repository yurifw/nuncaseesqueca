import requests
import re
import json
import codecs
from datetime import datetime, timezone
from lxml import html

def get_resource_uri():
	response = requests.get("https://g1.globo.com/")
	page_text = response.text
	regex = 'SETTINGS.BASTIAN\["RESOURCE_URI"\]="(.*)"'
	r = re.search(regex, page_text)
	if r is None:
		raise Exception("couldn't find resource uri on g1.globo.com")
	uri = r.group(1)
	return uri

def search_archive(term, begining, ending):
	url = "https://g1.globo.com/busca/?q={search}&ps=on&page={page}&order=recent&species=notícias&from=2016-11-01T00:00:00-0200&to=2021-03-06T23:59:59-0300"
	resp = requests.get(url.format(search='lava jato', page='1'))
	with codecs.open('trash.html','w','utf8') as f:
		f.write(resp.text)


def get_text_content(tree, xpaths):
	tag = None
	for xpath in xpaths:
		try:
			tag = tree.xpath(xpath)[0]
			break
		except:
			continue
	if tag is None:
		return ''
	
	xpaths_to_remove = ["//script", "//style", "//noscript", "//iframe", "//figure", "//div[@class='mc-article-share']", "//ul[@class='content-unordered-list']", "//div[@class='content-media__container']"]
	
	for xpath in xpaths_to_remove:
		for element in tree.xpath(xpath):
			element.getparent().remove(element)

	text = tag.text_content().strip()
	text = re.sub(r'\s{2,}', '\n', text)
	return text


def scrape_url(url):
	result = {'headline':'', 'text':'', 'url':'', 'publication_date':'', 'tags':[], 'publisher':'G1', 'scraped_date':datetime.now()}
	resp = requests.get(url)
	tree = html.fromstring(resp.text)
	
	headline_xpaths = ["//h1[@itemprop='headline']", "//h1[contains(@class, 'article-title')]"]
	headline = get_text_content(tree, headline_xpaths)
	if headline == '':
		print("no headline for %s" % url)

	text_xpaths = ["//article[@itemprop='articleBody']", "//div[@class='mc-article-body']"]
	text = get_text_content(tree, text_xpaths)
	if text == '':
		print("no text for %s" % url)

	publication_date_xpaths = ["//time[@itemprop='datePublished']", "//time[@class='published']"]
	publication_date = get_text_content(tree, publication_date_xpaths)
	if publication_date == '':
		print("no publication date for %s" % url)
	else:
		publication_date = datetime.strptime(publication_date, '%d/%m/%Y %Hh%M')
	
	result['headline'] = headline
	result['text'] = text
	result['url'] = url
	result['publication_date'] = publication_date
	result['is_relevant'] = None

	if result['text'] == '' or result['headline'] == '' or result['publication_date'] == '':
		return None
	return result	

def filter_metadata(metadata):
	ignored_types = ['post-playlist', 'post-shortz', 'webstories-agregador']
	ignored_domains = [
		'ge.globo.com', 'climatempo.com.br', '/podcast/educacao-financeira', '/guia-de-compras/', 'globo.com/fantastico', 
		'globo.com/pop-arte/', 'globo.com/podcast/'
	]
	
	filtered = []
	for m in metadata:
		ignore = False
		if m['type'] in ignored_types:
			ignore = True
			continue

		for domain in ignored_domains:
			if domain in m['content']['url']:
				ignore = True

		if ignore:
			continue
		else:
			filtered.append(m)
	
	return filtered


def scrape_news():
	PAGES_TO_SCRAPE = 6
	uri = get_resource_uri()
	result = []
	

	for i in range(1,PAGES_TO_SCRAPE+1):
		url = "%s/page/%s"%(uri,i)
		news_metadata = requests.get(url)
		news_metadata = json.loads(news_metadata.text)
		news_metadata = filter_metadata(news_metadata['items'])
		for metadata in news_metadata:
			r = scrape_url(metadata['content']['url'])
			if r is not None:
				result.append(r)
		
	print("G1 scraped.")
	return result

if __name__ == '__main__':
	search_archive('term', 'begining', 'ending')
