import requests
import json
from datetime import datetime


def scrape_news():
	result = []

	news = requests.get("https://www.bbc.com/portuguese/mostread.json")
	news = json.loads(news.text)
	for new in news['records']:
		publication = datetime.fromtimestamp(int(new['promo']['timestamp']) / 1000)
		result.append({
			"publicationDate": publication,
			"headline": new['promo']['headlines']['headline'],
			"url": "https://www.bbc.com%s" % new['promo']['locators']['assetUri'],
			"summary": new['promo']['summary']
		})

	return result
